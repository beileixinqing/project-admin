const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate')

// 表格模板
const TableModelSchema = new Schema({
    tableName: String,
    instruction: String,
    //状态 是否生效展示0为失效 1为生效
    status:{type: String, default: '1'},
    createUserName: String,
    updateUserName: String,
    createUserId:String,
    updateUserId: String,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now},
    table: [{
        headerName: String,
        fieldName:String,
        fieldType: String,
    }],
    //type:
    // 文本 text
    // 文件 file
})

TableModelSchema.plugin(mongoosePaginate)

mongoose.model('TableModel', TableModelSchema)
