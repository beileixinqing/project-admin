const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate')

// 项目模板
const ProjectModelSchema = new Schema({
    title: String,
    instruction: String,
    statusList: [{
        statusName: String,
        statusInfo: String,
        statusOrder:String,
        formModels: [{_id: Schema.Types.ObjectId,formName:String}],
        tableModels: [{_id: Schema.Types.ObjectId,tableName:String}]
    }],
    createUserName: String,
    updateUserName: String,
    createUserId:String,
    updateUserId: String,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now},
    //状态 是否生效展示0为失效 1为生效
    status:{type: String, default: '1'}

})

ProjectModelSchema.plugin(mongoosePaginate)

mongoose.model('ProjectModel', ProjectModelSchema)
