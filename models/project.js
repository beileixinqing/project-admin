const mongoose = require('mongoose')
const Schema = mongoose.Schema
// const ProjectModelSchema = require('./projectModel')
// const FormModelSchema = require('./formModel')
const mongoosePaginate = require('mongoose-paginate')

//项目
const ProjectSchema = new Schema({
    projectName: String,
    instruction:String,
    projectModelId: Schema.Types.ObjectId,
    projectModelName: String,
    statusId: Schema.Types.ObjectId,
    statusName: String,
    formModels: [{
        _id: Schema.Types.ObjectId,
        formName:String,
    }],
    tableModels: [{
        _id: Schema.Types.ObjectId,
        tableName:String,
    }],
    createUserName: String,
    updateUserName: String,
    createUserId:String,
    updateUserId: String,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now}
})

ProjectSchema.plugin(mongoosePaginate)

mongoose.model('Project', ProjectSchema)
