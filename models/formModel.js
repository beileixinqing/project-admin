const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate')

// 表单模板
/**
*fieldType:
单行文本框 (input)text
多行文本框 textarea
数字 (input)number
邮箱地址 (input)email
日期 (input)date
单选按钮  (input)radio
复选框 (input)checkbox
下拉列表框 select
表格 table
文件 (input)file(url)
*/
const FormModelSchema = new Schema({
    formName: String,
    instruction: String,
    //状态 是否生效展示0为失效 1为生效
    status:{type: String, default: '1'},
    createUserName: String,
    updateUserName: String,
    createUserId:String,
    updateUserId: String,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now},
    formFields: [{
        fieldid:String,
        displayName: String,
        // fieldName:String,
        fieldType: String,
        placeholder:String,
        isRequired:{type: Boolean, default: false},
        column:String,
        fieldOptions:[],
        fieldOrder:Number
        //图片 or 文件  限制图片/文件大小
        // maxSize:String, 文字长度
    }]
})
// Defines a pre hook for the document.
FormModelSchema.pre('save', function (next) {
    if (this.isNew) {
        this.createAt = this.updateAt = Date.now()
    }
    else {
        this.updateAt = Date.now()
    }
    next()
})
FormModelSchema.plugin(mongoosePaginate)

mongoose.model('FormModel', FormModelSchema)
