const mongoose = require('mongoose')
const Schema = mongoose.Schema
// const ProjectModelSchema = require('./projectModel')
// const FormModelSchema = require('./formModel')
const mongoosePaginate = require('mongoose-paginate')

//项目
const ProjectDataSchema = new Schema({
    //项目id
    projectId: String,
    //子模块id（目前表单id和表格id）
    projectSubmoduleId: String,
    //json数据
    data:String,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now},
    createUserId: String,
    updateUserId: String,
    createUserName: String,
    updateUserName: String,
})

ProjectDataSchema.plugin(mongoosePaginate)

mongoose.model('ProjectData', ProjectDataSchema)
