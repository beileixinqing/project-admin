const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate')

const UserSchema = new Schema({
    userId:Schema.Types.ObjectId,
    username: String,
    mobile: String,
    password: String,
    //备注
    remarks: String,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now},
    //用户类型
    userType: String,
    createUserId: Schema.Types.ObjectId,
    createUserName: String,
    updateUserId: Schema.Types.ObjectId,
    updateUserName: String
})

UserSchema.plugin(mongoosePaginate)

module.exports=mongoose.model('User', UserSchema)
