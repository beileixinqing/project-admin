#!/usr/bin/env node

/**
 * Module dependencies.
 */

const app = require('../app')
const config = require('../config/config')
const http = require('http');
const path = require('path');

const appDir = path.resolve(__dirname, '..')
const logDir = path.join(appDir, 'logs')
/**
 * make a log directory, just in case it isn't there.
 */
try {
    require('fs').mkdirSync(logDir)
} catch (e) {
    if (e.code !== 'EEXIST') {
        console.error('Could not set up log directory, error was: ', e)
        process.exit(1)
    }
}

const log4js = require('koa-log4')
log4js.configure(path.join(appDir, 'log4js.json'), { cwd: logDir })
const logger = log4js.getLogger('startup')
/**
 * Get port from environment and store in Express.
 */

const port = config.port
// const port = normalizePort(process.env.PORT || '3000');
// app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app.callback());
/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(config.port , function () {
    logger.info('Koa server listening on port ' + config.port);
})
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    let bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            logger.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    let addr = server.address();
    let bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    logger.debug('Listening on ' + bind);
}
