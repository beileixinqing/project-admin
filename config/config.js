const path = require('path')
const rootPath = path.normalize(__dirname + '/..')
const env = process.env.NODE_ENV || 'development'

const config = {
    development: {
        root: rootPath,
        app: {
            name: 'koa'
        },
        port: process.env.PORT || 9990,
        db: 'mongodb://localhost/koa-development'
    },

    test: {
        root: rootPath,
        app: {
            name: 'koa'
        },
        port: process.env.PORT || 9991,
        db: 'mongodb://localhost/koa-test'
    },

    production: {
        root: rootPath,
        app: {
            name: 'koa'
        },
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost/koa-production'
    }
}

module.exports = config[env]
