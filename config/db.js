const mongoose = require('mongoose')
const config = require('./config')
const glob = require('glob')
const log4js = require('koa-log4')
const logger = log4js.getLogger('db')

mongoose.connect(config.db)
const db = mongoose.connection
db.on('error', logger.error.bind(console, 'connection error:'))
db.once('open', function () {
    logger.info('mongodb connected!')
})

const models = glob.sync(config.root + '/models/*.js')
models.forEach(function (model) {
    require(model)
})

module.exports = {
    mongoose: mongoose,
    db: db
}