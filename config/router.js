const router = require('koa-router')()
//controllers
const User = require('../controllers/user')
const TableModel = require('../controllers/tableModel')
const FormModel = require('../controllers/formModel')
const ProjectModel = require('../controllers/projectModel')
const Project = require('../controllers/project')
const ProjectData = require('../controllers/projectData')

module.exports = function(){
    // user
    //登录页面
    router.get('/login', User.renderLogin)
    //渲染新增员工/用户页面
    router.get('/user/add',User.renderAddUser)
    //渲染用户/员工列表页面
    router.get('/userList', User.renderUserList)
    //渲染用户/员工编辑页面
    router.get('/user/update', User.renderUpdateUser)
    //渲染修改密码
    router.get('/user/modifyPwd', User.renderModifyPwd)

    //新增员工api
    router.post('/api/user/add',User.addUser)
    //更新员工信息api
    router.post('/api/user/update',User.updateUser)
    //删除员工api
    router.post('/api/user/delete',User.deleteUser)
    //删除多个员工api
    router.post('/api/user/deleteMany',User.deleteManyUser)
    //展示员工信息
    router.post('/api/user/info',User.userInfo)
    //登录api
    router.post('/api/login', User.login)
    //退出登录api
    router.get('/api/logout', User.logout)
    //修改密码api
    router.post('/api/user/modifyPwd', User.modifyPwd)
    //分页查询用户列表
    router.post('/api/user/list',User.list)


    // table
    //渲染创建表格模版页面
    router.get('/createTableModel',TableModel.renderTableModel)
    //渲染编辑表格模版路由页面
    router.get('/updateTableModel',TableModel.renderUpdateTableModel)
    //渲染表格模版列表页面
    router.get('/tableModelList',TableModel.renderTableModelList)

    //渲染创建表格模版api
    router.post('/api/tableModel/create',TableModel.createTableModel)
    //更新表格模板api
    router.post('/api/tableModel/update',TableModel.updateTableModel)
    //迭代更新表格模板api
    router.post('/api/tableModel/updateIteration',TableModel.updateIterationTableModel)
    //删除表格模板api
    router.get('/api/tableModel/delete',TableModel.deleteTableModel)
    //删除多个表格模板api
    router.post('/api/tableModel/deleteMany',TableModel.deleteManyTableModel)
    //通过id数组获取多个表格模板信息api
    router.post('/api/tableModel/tableModelInfos',TableModel.tableModelInfos)
    //展示表格模版信息
    router.post('/api/tableModel/tableModelInfo',TableModel.tableModelInfo)
    //分页查询表格列表
    router.post('/api/tableModel/list',TableModel.list)


    // form
    //渲染创建表单模版路由页面
    router.get('/createFormModel',FormModel.renderFormModel)
    //渲染编辑表单模版路由页面
    router.get('/updateFormModel',FormModel.renderUpdateFormModel)
    //渲染表单模版列表路由页面
    router.get('/formModelList',FormModel.renderFormModelList)
    //渲染表单文件预览页面
    router.get('/pre',FormModel.preview)

    //创建表单模版api
    router.post('/api/formModel/create',FormModel.createFormModel)
    //更新表单模板api
    router.post('/api/formModel/update',FormModel.updateFormModel)
    //迭代更新表单模板api
    router.post('/api/formModel/updateIteration',FormModel.updateIterationFormModel)
    //删除表单模板api
    router.get('/api/formModel/delete',FormModel.deleteFormModel)
    //删除多个表单模板api
    router.post('/api/formModel/deleteMany',FormModel.deleteManyFormModel)
    //展示表单模版信息
    router.post('/api/formModel/info',FormModel.formModelInfo)
    //分页查询表单列表
    router.post('/api/formModel/list',FormModel.list)


    // projectModel
    //渲染创建项目模版路由页面
    router.get('/createProjectModel',ProjectModel.renderProjectModel)
    //渲染编辑项目模版路由页面
    router.get('/updateProjectModel',ProjectModel.renderUpdateProjectModel)
    //渲染项目模版列表路由页面
    router.get('/projectModelList',ProjectModel.renderProjectModelList)
    //条件查询
    router.get('/find',ProjectModel.find)

    //创建项目模版api
    router.post('/api/projectModel/create',ProjectModel.createProjectModel)
    //更新项目模版api
    router.post('/api/projectModel/update',ProjectModel.updateProjectModel)
    //迭代更新
    router.post('/api/projectModel/updateIteration',ProjectModel.updateIterationProjectModel)
    //删除项目模版api
    router.get('/api/projectModel/delete',ProjectModel.deleteProjectModel)
    //删除多个项目模版api
    router.post('/api/projectModel/deleteMany',ProjectModel.deleteManyProjectModel)
    //展示项目模版信息
    router.post('/api/projectModel/info',ProjectModel.projectModelInfo)
    //分页查询项目模板列表
    router.post('/api/projectModel/list',ProjectModel.list)

    // project
    //渲染创建项目路由页面
    router.get('/createProject',Project.renderProject)
    //渲染编辑项目路由页面
    router.get('/projectList',Project.renderProjectList)
    //渲染项目列表路由页面
    router.get('/updateProject',Project.renderUpdateProject)

    //通过项目id查询出模版信息，表单模版信息，表格模版信息，以及数据
    router.get('/api/project/findAllById',Project.findAllById)
    //创建项目api
    router.post('/api/project/create',Project.createProject)
    //渲染状态流程列表api
    router.get('/api/project/renderStatusList',Project.renderStatusList)
    //渲染表单模板api
    router.get('/api/project/renderFormModel',Project.renderFormModel)
    //渲染表格模板api
    router.get('/api/project/renderTableModel',Project.renderTableModel)
    //更新项目api
    router.post('/api/project/update',Project.updateProject)
    //删除项目api
    router.get('/api/project/delete',Project.deleteProject)
    //删除多个项目api
    router.post('/api/project/deleteMany',Project.deleteManyProject)
    //展示项目信息
    router.post('/api/project/info',Project.projectInfo)
    //分页查询项目列表
    router.post('/api/project/list',Project.list)
    //上传文件
    router.post('/api/project/uploadFile',Project.uploadFile)
    //删除文件
    router.post('/api/project/deleteFile',Project.deleteFile)
    //下载文件
    router.post('/api/project/downloadFile',Project.downloadFile)
    //下载文件
    router.get('/download',Project.download)

    //插入单条数据
    router.post('/api/projectData/add',ProjectData.add)
    //删除单条数据
    router.get('/api/projectData/delete',ProjectData.delete)
    //删除多条数据
    router.post('/api/projectData/deleteMany',ProjectData.deleteMany)
    //查询数据
    router.post('/api/projectData/find',ProjectData.find)

    return router
}


