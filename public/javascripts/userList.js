$(function () {
    let totalPages=0;
    let currentPage=1;
    let numberOfPages=8;
    $.ajax({
        url: '/api/user/list',
        type: 'POST',
        data: {page: 1, size: 10},
        success: function (result) {
            $('#userList tbody').empty();
            let tableListItem="";
            if(result.total<=0){
                tableListItem+=`<tr>
                    <td colspan="8" style="text-align: center">暂无数据</td>
                    </tr>`;
                $('#userList tbody').append(tableListItem);
                return false;
            }
            for(let l of result.list){
                let createUserName=l.createUserName?l.createUserName:"";
                let updateUserName=l.updateUserName?l.updateUserName:"";
                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD:HH:MM:SS'):"";
                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD:HH:MM:SS'):"";
                tableListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.username}</td>
                    <td>${l.mobile}</td>
                    <td>${l.remarks}</td>
                    <!--<td>${l.userType}</td>-->
                    <td>${createAt}</td>
                    <td>${updateAt}</td>
                    <td>
                        <a href="/user/update?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <a style="margin-bottom:5px;margin-right:10px;" onclick="previewUser('${l._id}')" class="btn btn-info"> 查看</a>
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','user')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
            }
            $('#userList tbody').append(tableListItem);
            totalPages=result.totalPages;
            currentPage=result.currentPage;
            // $('.totalPages').text(result.totalPages)
            $('#pageLimit').bootstrapPaginator({
                currentPage: currentPage,
                totalPages: totalPages,
                size:'normal',
                bootstrapMajorVersion: 3,
                alignment:"left",
                numberOfPages:numberOfPages,
                itemTexts: function (type, page, current) {
                    switch (type) {
                        case "first": return "首页";
                        case "prev": return "上一页";
                        case "next": return "下一页";
                        case "last": return "末页";
                        case "page": return page;
                    }//默认显示的是第一页。
                },
                onPageClicked: function (event, originalEvent, type, page) {//给每个页眉绑定一个事件，其实就是ajax请求，其中page变量为当前点击的页上的数字。
                    $.ajax({
                        url: '/api/user/list',
                        type: 'POST',
                        data: {'page': page, 'size':10},
                        success: function (result) {
                            $('#userList tbody').empty();
                            let tableListItem="";
                            for(let l of result.list){
                                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                tableListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.username}</td>
                    <td>${l.mobile}</td>
                    <td>${l.remarks}</td>
                    <!--<td>${l.userType}</td>-->
                    <td>${createAt}</td>
                    <td>${updateAt}</td>
                    <td>
                        <a href="/user/update?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <a style="margin-bottom:5px;margin-right:10px;" onclick="previewUser('${l._id}')" class="btn btn-info"> 查看</a>
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','user')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
                            }
                            $('#userList tbody').append(tableListItem);
                            // $('.totalPages').text(result.totalPages)
                        }
                    })
                }
            });
        }
    })
});
function previewUser(id) {
    $.ajax({
        type : "POST",
        url : "/api/user/Info",
        data : {
            id:id
        },
        success : function(result) {
            let html='';
            let createAt=result['createAt']?moment(result['createAt']).format('YYYY-MM-DD HH:MM:SS'):"";
            let updateAt=result['updateAt']?moment(result['updateAt']).format('YYYY-MM-DD HH:MM:SS'):"暂无更新";
            let userType=result['userType']?result['userType']:"";
            html+=`<p><span>用户名：</span>${result['username']}</p>`;
            html+=`<p><span>手机号：</span>${result['mobile']}</p>`;
            html+=`<p><span>备注：</span>${result['remarks']}</p>`;
            html+=`<p><span>创建时间：</span>${createAt}</p>`;
            html+=`<p><span>更新时间：</span>${updateAt}</p>`;
            // html+=`<p><span>用户类型：</span>${userType}</p>`;
            $("#preview .modal-body").html(html);
            $("#preview"). modal();
        },
        error:function (result) {
            $("#myModal"). modal('hide');
        }
    });
}
