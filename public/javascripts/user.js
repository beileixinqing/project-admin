//表单验证初始化
$(function () {
    $('form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '用户名不能为空'
                    }
                }
            },
            mobile: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '手机号不能为空'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    }
                }
            },
            password2: {
                validators: {
                    notEmpty: {
                        message: '请再次输入密码'
                    }
                }
            },
            newPassword: {
                validators: {
                    notEmpty: {
                        message: '请输入新密码'
                    }
                }
            },
            newPassword2: {
                validators: {
                    notEmpty: {
                        message: '请再次输入新密码'
                    }
                }
            }
        }
    });
});

//添加员工
$("input").focus(function(){
    $(this).next(".form-alert").empty();
    $(this).next(".form-alert").hide();
})
function addUser() {
    $("#addUser").data("bootstrapValidator").validate();
    let validFlag = $("#addUser").data("bootstrapValidator").isValid();
    if(validFlag){
        let data={};
        $.each($("#addUser").serializeArray(),function () {
            data[this.name]=this.value;
        })
        let username=$("input[name='username']").val();
        let password=$("input[name='password']").val();
        let password2=$("input[name='password2']").val();
        let mobile=$("input[name='mobile']").val();
        let remarks=$("input[name='remarks']").val();
        let mobileReg=/^1[3|4|5|8][0-9]\d{4,8}$/i;
        if(username.length==0){
            let formAlert=$("input[name='username']").next(".form-alert");
            formAlert.show();
            formAlert.html("用户名不可以为空");
            return false
        }
        if(mobile.length==0){
            let formAlert=$("input[name='mobile']").next(".form-alert");
            formAlert.show();
            formAlert.html("手机号不可以为空");
            return false
        }else if(!mobileReg.test(mobile)){
            let formAlert=$("input[name='mobile']").next(".form-alert");
            formAlert.show();
            formAlert.html("手机号格式不正确");
            return false
        }
        if(password.length==0){
            let formAlert=$("input[name='password']").next(".form-alert");
            formAlert.show();
            formAlert.html("密码不可以为空");
            return false
        }
        if(password2.length==0){
            let formAlert=$("input[name='password2']").next(".form-alert");
            formAlert.show();
            formAlert.html("请再次输入密码");
            return false
        }else if(password2!==password){
            let formAlert=$("input[name='password2']").next(".form-alert");
            formAlert.show();
            formAlert.html("两次密码输入不一致");
            return false
        }
        if(remarks.length==0){
            let formAlert=$("input[name='remarks']").next(".form-alert");
            formAlert.show();
            formAlert.html("备注信息不可以为空");
            return false
        }
        setTimeout(function () {
            $.ajax({
                type : "POST",
                url : "/api/user/add",
                data :data,
                success : function(result) {
                    $(".alert").show();
                    if(result.status==1){
                        $(".alert").addClass('alert-success').removeClass('alert-danger');
                    }
                    $(".alert").text(result.msg);
                    setCookie('username',result.data.username,'d9999')
                    setCookie('userId',result.data._id,'d9999')
                    setTimeout(function () {
                        $(".alert").hide();
                        window.location.href='/userList'
                    },1000)
                },
                error:function (result) {
                    $(".alert").show();
                    $(".alert").text("注册失败，请重试");
                    setTimeout(function () {
                        $(".alert").hide();
                    },3000)
                }
            });
        },100)
    }
}
function updateUser() {
    $("#updateUser").data("bootstrapValidator").validate();
    let validFlag = $("#updateUser").data("bootstrapValidator").isValid();
    if(validFlag){
        let data={}
        $.each($("#updateUser").serializeArray(),function () {
            data[this.name]=this.value;
        })
        let username=$("input[name='username']").val();
        let mobile=$("input[name='mobile']").val();
        let remarks=$("input[name='remarks']").val();
        let mobileReg=/^1[3|4|5|8][0-9]\d{4,8}$/i;
        if(username.length==0){
            let formAlert=$("input[name='username']").next(".form-alert");
            formAlert.show();
            formAlert.html("用户名不可以为空");
            return false
        }
        if(mobile.length==0){
            let formAlert=$("input[name='mobile']").next(".form-alert");
            formAlert.show();
            formAlert.html("手机号不可以为空");
            return false
        }else if(!mobileReg.test(mobile)){
            let formAlert=$("input[name='mobile']").next(".form-alert");
            formAlert.show();
            formAlert.html("手机号格式不正确");
            return false
        }
        if(remarks.length==0){
            let formAlert=$("input[name='remarks']").next(".form-alert");
            formAlert.show();
            formAlert.html("备注信息不可以为空");
            return false
        }
        data.id=getCookie("userId");
        setTimeout(function () {
            $.ajax({
                type : "POST",
                url : "/api/user/update",
                data :data,
                success : function(result) {
                    console.log(result)
                    $(".alert").show();
                    if(result.status==1){
                        $(".alert").addClass('alert-success').removeClass('alert-danger');
                    }
                    $(".alert").text(result.msg);
                    setCookie('username',result.data.username,'d9999')
                    setCookie('userId',result.data._id,'d9999')
                    setTimeout(function () {
                        $(".alert").hide();
                        window.location.href='/userList'
                    },1000)
                },
                error:function (result) {
                    $(".alert").show();
                    $(".alert").text("修改失败，请重试");
                    setTimeout(function () {
                        $(".alert").hide();
                    },3000)
                }
            });
        },100)
    }
}
function modifyPwd() {
    $("#modifyUser").data("bootstrapValidator").validate();
    let validFlag = $("#modifyUser").data("bootstrapValidator").isValid();
    if(validFlag){
        let data={}
        $.each($("#modifyUser").serializeArray(),function () {
            data[this.name]=this.value;
        })
        let password=$("input[name='password']").val();
        let newPassword=$("input[name='newPassword']").val();
        let newPassword2=$("input[name='newPassword2']").val();
        if(password.length==0){
            let formAlert=$("input[name='password']").next(".form-alert");
            formAlert.show();
            formAlert.html("原密码不可以为空");
            return false
        }
        if(newPassword.length==0){
            let formAlert=$("input[name='newPassword']").next(".form-alert");
            formAlert.show();
            formAlert.html("新密码不可以为空");
            return false
        }
        if(newPassword.length==password.length){
            let formAlert=$("input[name='newPassword']").next(".form-alert");
            formAlert.show();
            formAlert.html("新密码不可与原密码相同");
            return false
        }
        if(newPassword2.length==0){
            let formAlert=$("input[name='newPassword2']").next(".form-alert");
            formAlert.show();
            formAlert.html("请再次输入新密码");
            return false
        }else if(newPassword2!==newPassword){
            let formAlert=$("input[name='newPassword2']").next(".form-alert");
            formAlert.show();
            formAlert.html("新密码两次输入不一致");
            return false
        }
        data.id=getCookie("userId");
        setTimeout(function () {
            $.ajax({
                type : "POST",
                url : "/api/user/modifyPwd",
                data :data,
                success : function(result) {
                    $(".alert").show();
                    if(result.status==1){
                        $(".alert").addClass('alert-success').removeClass('alert-danger');
                    }
                    $(".alert").text(result.msg);
                    setTimeout(function () {
                        $(".alert").hide();
                        window.location.href='/userList'
                    },1000)
                },
                error:function (result) {
                    $(".alert").show();
                    $(".alert").text("密码修改失败，请重试");
                    setTimeout(function () {
                        $(".alert").hide();
                    },3000)
                }
            });
        },100)
    }
}
//登录
function login() {
    $("#login").data("bootstrapValidator").validate();
    let validFlag = $("#login").data("bootstrapValidator").isValid();
    if(validFlag){
        let data={}
        $.each($("#login").serializeArray(),function () {
            data[this.name]=this.value;
        })
        let remember=$("input[name='remember']").val();
        let username=$("input[name='username']").val();
        let password=$("input[name='password']").val();
        if(username.length==0){
            let formAlert=$("input[name='username']").next(".form-alert");
            formAlert.show();
            formAlert.html("用户名不可以为空");
            return false
        }
        if(password.length==0){
            let formAlert=$("input[name='password']").next(".form-alert");
            formAlert.show();
            formAlert.html("密码不可以为空");
            return false
        }
        setTimeout(function () {
            $.ajax({
                type : "POST",
                url : "/api/login",
                data :data,
                success : function(result) {
                    console.log(result)
                    $(".alert").show();
                    if(result.msg=='success'){
                        $(".alert").addClass('alert-success').removeClass('alert-danger');
                        setCookie('username',result.data.username,'d9999')
                        setCookie('userId',result.data._id,'d9999')
                    }
                    console.log(result.info)
                    $(".alert").text(result.info);
                    setTimeout(function () {
                        $(".alert").hide();
                        window.location.href='/projectList'
                        /*if(result.url==='/'){
                         window.location.href='/projectList'
                         }else{
                         window.location.href=result.url
                         }*/
                    },1000)
                },
                error:function (result) {
                    $(".alert").show();
                    $(".alert").text(result.info);
                    setTimeout(function () {
                        $(".alert").hide();
                    },3000)
                }
            });
        },100)
    }
}
