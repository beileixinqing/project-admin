//编辑项目模版页面默认加载状态列表数据
let data=new Array();
let statusList=new Array();
let tableId=0;
//表单验证初始化
$(function () {
    $('#projectModel').bootstrapValidator({
        // message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '项目模板名称不能为空'
                    }
                }
            },
            instruction: {
                validators: {
                    notEmpty: {
                        message: '项目模板介绍不能为空'
                    }
                }
            },
            projectModel: {
                validators: {
                    notEmpty: {
                        message: '请选择项目模板'
                    }
                }
            }
        }
    });
    $('#status').bootstrapValidator({
        // message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            statusOrder: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '项目模板名称不能为空'
                    }
                }
            },
            statusName: {
                validators: {
                    notEmpty: {
                        message: '项目模板介绍不能为空'
                    }
                }
            },
            statusInfo: {
                validators: {
                    notEmpty: {
                        message: '请选择项目模板'
                    }
                }
            }
        }
    });
    $('#statusEdit').bootstrapValidator({
        // message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            statusOrder: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '项目模板名称不能为空'
                    }
                }
            },
            statusName: {
                validators: {
                    notEmpty: {
                        message: '项目模板介绍不能为空'
                    }
                }
            },
            statusInfo: {
                validators: {
                    notEmpty: {
                        message: '请选择项目模板'
                    }
                }
            }
        }
    });
});

function clone(arr) {
    var len = arr.length,
        arr = [];
    for(var i = 0;i < len;i++) {
        if(typeof arr[i] !== "object") {
            arr.push(arr[i]);
        } else {
            arr.push(arr[i].clone());
        }
    }
    return arr;
}
if($("#projectModelInfo").val()){
    statusList=JSON.parse($("#projectModelInfo").val()).statusList;
    for(let s of statusList){
        tableId+=1
        s['tableId']=tableId
    }
    data=$.extend(true, [], statusList);
    for(let value of data){
        delete value._id;
        for(let key in value){
            if(Array.isArray(value[key])){
                if(key==='formModels'){
                    value.formModel="";
                    for(let v2 of value[key]){
                        value.formModel+="《"+v2.formName+"》"+" ";
                    }
                }else if(key==='tableModels'){
                    value.tableModel="";
                    for(let v2 of value[key]){
                        value.tableModel+="《"+v2.tableName+"》"+" ";
                    }
                }
            }
        }
        delete value.formModels;
        delete value.tableModels;
        value.tool=`<a style="margin-bottom:5px;" onclick="removeStatus(this,${value.tableId})" class="btn btn-default"> 删除</a>
                    <a style="margin-bottom:5px;" onclick="editStatusModal(this,${value.tableId})" class="btn btn-info"> 编辑</a>`;
    }
}
let t = $('#projectModelTable').DataTable({
    data:data,
    columns: [
        // {
        //     data: 'checked', // 返回json数据中的name
        //     title: '选择', // 表格表头显示文字
        // },
        {
            data: 'tableId', // 返回json数据中的name
            title: 'tableId', // 表格表头显示文字
            visible:false,
        },
        {
            data: 'statusOrder', // 返回json数据中的name
            title: '流程顺序', // 表格表头显示文字
        },
        {
            data: 'statusName', // 返回json数据中的name
            title: '流程名称', // 表格表头显示文字
            orderable:false,
        },
        {
            data: 'statusInfo', // 返回json数据中的name
            title: '流程说明', // 表格表头显示文字
            orderable:false,
        },
        {
            data: 'formModel', // 返回json数据中的name
            title: '流程需填写表单', // 表格表头显示文字
            orderable:false,
        },
        {
            data: 'tableModel', // 返回json数据中的name
            title: '流程需填写表格', // 表格表头显示文字
            orderable:false,
        },
        {
            data: 'tool', // 返回json数据中的name
            title: '操作', // 表格表头显示文字
            orderable:false,
        },
    ]
});
//状态排序
let compare = function (obj1, obj2) {
    var val1 = obj1.statusOrder;
    var val2 = obj2.statusOrder;
    if (val1 < val2) {
        return -1;
    } else if (val1 > val2) {
        return 1;
    } else {
        return 0;
    }
}
//表单重置
$("#btn_add").click(function () {
    $('#addModal').modal();
    $("#status")[0].reset();
})
function addStatus() {
    tableId+=1;
    $("#status").data("bootstrapValidator").validate();
    let validFlag = $("#status").data("bootstrapValidator").isValid();
    if(validFlag){
        let formModels=new Array();
        let tableModels=new Array();
        let formModelText="";
        $("#formModels :checkbox:checked").each(function(index,value){
            let formModel={};
            formModel._id = $(this).val();
            formModelText=formModelText+' 《'+$(value).next('span').text()+'》';
            formModel.formName = $(this).next("span").text();
            formModels[index]=formModel;
        });
        let tableModelText="";
        $("#tableModels :checkbox:checked").each(function(index,value){
            let tableModel={};
            tableModel._id = $(this).val();
            tableModelText=tableModelText+' 《'+$(value).next('span').text()+'》';
            tableModel.tableName = $(this).next("span").text();
            tableModels[index]=tableModel;
        });
        let data1 = {
            // checked:`<input type="checkbox" value=${tableId}/>`,
            tableId:tableId,
            statusName: $('#statusName').val(),
            statusInfo: $('#statusInfo').val(),
            statusOrder:$('#statusOrder').val(),
            _csrf:$('input[name="_csrf"]').val(),
            formModel: formModelText,
            tableModel: tableModelText,
            tool : `<a style="margin-bottom:5px;" onclick="removeStatus(this,tableId)" class="btn btn-default"> 删除</a>
                    <a style="margin-bottom:5px;" onclick="editStatusModal(this,tableId)" class="btn btn-info"> 编辑</a>`
        };
        let data2 = {
            tableId:tableId,
            statusName: $('#statusName').val(),
            statusInfo: $('#statusInfo').val(),
            statusOrder:$('#statusOrder').val(),
            _csrf:$('input[name="_csrf"]').val(),
            formModels: formModels,
            tableModels: tableModels
        };
        statusList.push(data2);
        console.log(data1)
        t.row.add(data1).draw( false );
        $('#addModal').modal('hide');
    }
}
function createProjectModel() {
    $("#projectModel").data("bootstrapValidator").validate();
    let validFlag = $("#projectModel").data("bootstrapValidator").isValid();
    if(validFlag){
        if(statusList.length<=0){
            $('#projectModelTable_wrapper').after(`<small class="help-block" style="color:#a94442;">流程不能为空</small>`);
            return false
        }
        let title=$('#projectModel input[name="title"]').val();
        let instruction=$('#projectModel textarea[name="instruction"]').val();
        let _csrf=$('#projectModel input[name="_csrf"]').val();
        statusList.sort(compare);
        for(let s of statusList){
            delete s['tableId']
        }
        const projectModelData={
            title:title,
            instruction:instruction,
            _csrf:_csrf,
            statusList:statusList,
            createUserName:getCookie('username'),
            createUserId:getCookie('userId'),
            status:1
        }
        console.log(projectModelData)
        $.ajax({
            type : "POST",
            url : "/api/projectModel/create",
            data : projectModelData,
            success : function(result) {
                $('#alert-success').show();
                setTimeout(()=>{
                    $('#alert-success').hide();
                    window.location.href="/projectModelList"
                },2000)
            },
            error:function (result) {
                $('#alert-alert').show();
                setTimeout(()=>{
                    $('#alert-alert').hide();
                },3000)
            }
        });
    }
}
function updateProjectModel() {
    $("#projectModel").data("bootstrapValidator").validate();
    let validFlag = $("#projectModel").data("bootstrapValidator").isValid();
    if(validFlag){
        if(statusList.length<=0){
            $('#projectModelTable_wrapper').after(`<small class="help-block" style="color:#a94442;">表头字段不能为空</small>`);
            return false
        }
        let id=$('#projectModel input[name="id"]').val();
        let title=$('#projectModel input[name="title"]').val();
        let instruction=$('#projectModel textarea[name="instruction"]').val();
        let _csrf=$('#projectModel input[name="_csrf"]').val();
        let createUserName=$('#projectModel input[name="createUserName"]').val();
        let createUserId=$('#projectModel input[name="createUserId"]').val();
        statusList.sort(compare);
        for(let s of statusList){
            delete s['tableId']
        }
        const projectModelData={
            title:title,
            instruction:instruction,
            _csrf:_csrf,
            statusList:statusList,
            createUserName:createUserName,
            createUserId:createUserId,
            updateUserName:getCookie('username'),
            updateUserId:getCookie('userId'),
            status:1
        }
        console.log(projectModelData)
        $.ajax({
            type : "POST",
            url : "/api/projectModel/update",
            data : {
                id:id,
                status:0
            },
            success : function(result) {
                console.log(result)
            },
            error:function (result) {
                console.log(result)
            }
        });
        $.ajax({
            type : "POST",
            url : "/api/projectModel/updateIteration",
            data : projectModelData,
            success : function(result) {
                $('#alert-success').show();
                setTimeout(()=>{
                    $('#alert-success').hide();
                    window.location.href="/projectModelList"
                },2000)
            },
            error:function (result) {
                $('#alert-alert').show();
                setTimeout(()=>{
                    $('#alert-alert').hide();
                    window.location.href="/projectModelList"
                },2000)
            }
        });
    }
}
//删除单条status
function removeStatus(e,tableId) {
    $("deleteModal .modal-body p").html("确认删除该条数据吗？");
    $("#deleteModal"). modal();
    $("#delete").unbind('click').click(function () {
        $("#deleteModal"). modal('hide');
        deleteTr(e);
        for(let s of statusList){
            if(s['tableId']===tableId){
                statusList.remove(s)
            }
        }
    })
}
function deleteTr(e){
    t.row($(e).parents('tr')[0]).remove().draw(false);
}
//编辑单条status
function editStatusModal(e,id) {
    console.log("#########################")
    console.log(id)
    $('#editModal').modal();
    $("#editModal .modal-footer").empty();
    $("#statusEdit")[0].reset();
    for(let s of statusList){
        if(s['tableId']===id){
            $('#statusName2').val(s.statusName);
            $('#statusInfo2').val(s.statusInfo);
            $('#statusOrder2').val(s.statusOrder);
            for(let f of s.formModels){
                for(let v of $("#formModels2 .checkbox")){
                    if(f._id===$(v).find("label input").val()){
                        $(v).find("label input").prop("checked",true)
                    }
                }
            }
            for(let f of s.tableModels){
                for(let v of $("#tableModels2 .checkbox")){
                    if(f._id===$(v).find("label input").val()){
                        $(v).find("label input").prop("checked",true)
                    }
                }
            }
        }
    }
    $("#editModal .modal-footer").append(`<button type="button" data-dismiss="modal" class="btn btn-default">取消</button>
                                          <button type="button" id="editBtn" class="btn btn-primary">确认</button>`);
    $("#editBtn").unbind('click').click(function () {
        $("#statusEdit").data("bootstrapValidator").validate();
        let validFlag = $("#statusEdit").data("bootstrapValidator").isValid();
        if (validFlag) {
            let formModels = new Array();
            let tableModels = new Array();
            let formModelText = "";
            $("#formModels2 :checkbox:checked").each(function (index, value) {
                let formModel = {};
                formModel._id = $(this).val();
                formModelText = formModelText + ' 《' + $(value).next('span').text() + '》';
                formModel.formName = $(this).next("span").text();
                formModels[index] = formModel;
            });
            let tableModelText = "";
            $("#tableModels2 :checkbox:checked").each(function (index, value) {
                let tableModel = {};
                tableModel._id = $(this).val();
                tableModelText = tableModelText + ' 《' + $(value).next('span').text() + '》';
                tableModel.tableName = $(this).next("span").text();
                tableModels[index] = tableModel;
            });
            console.log("%%%%%%%%%%%%%%%%%%%")
            console.log(id)
            let data1 = {
                // checked:`<input type="checkbox" value=${tableId}/>`,
                tableId: id,
                statusName: $('#statusName2').val(),
                statusInfo: $('#statusInfo2').val(),
                statusOrder: $('#statusOrder2').val(),
                // _csrf: $('input[name="_csrf"]').val(),
                formModel: formModelText,
                tableModel: tableModelText,
                tool: `<a style="margin-bottom:5px;" onclick="removeStatus(this,${id})" class="btn btn-default"> 删除</a>
                <a style="margin-bottom:5px;" onclick="editStatusModal(this,${id})" class="btn btn-info"> 编辑</a>`
            };
            let data2 = {
                tableId: id,
                statusName: $('#statusName2').val(),
                statusInfo: $('#statusInfo2').val(),
                statusOrder: $('#statusOrder2').val(),
                // _csrf: $('input[name="_csrf"]').val(),
                formModels: formModels,
                tableModels: tableModels
            };
            for (let i = 0; i < statusList.length; i++) {
                if (statusList[i]['tableId'] === id) {
                    statusList[i] = data2;
                    console.log(statusList)
                }
            }
            t.row($(e).parents("tr")).data(data1).draw();
            $('#editModal').modal('hide');
        }
    })
}
//把datatable警告置空
$.fn.dataTable.ext.errMode = 'none';
