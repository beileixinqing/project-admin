//定义表单字段数组
let formFields=new Array();
let _csrf=$("input[name='_csrf']").val();
//编辑页面加载默认已选表单组件
if($("#formModelInfo").val()){
    formFields=JSON.parse($("#formModelInfo").val()).formFields;
    for(let f of formFields){
        let formDisplay='';
        if(f.fieldType.split("-")[0]=="input"){
            if(f.fieldType.split("-")[1]==="radio"){
                let option="";
                for(let o of f.fieldOptions){
                    option+=`<label class="radio-inline"><input type="radio" value=${o}>${o}</label>`
                }
                let leftHtml=`<div class="form-group ${f.column}" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="radio">`;
                let rightHtml=`</div></div>`;
                formDisplay=leftHtml+option+rightHtml;
                $('#formDisplay').append(formDisplay);
            }else if(f.fieldType.split("-")[1]==="checkbox"){
                let option="";
                for(let o of f.fieldOptions){
                    option+=`<label class="checkbox-inline"><input type="checkbox" value=${o}>${o}</label>`
                }
                let leftHtml=`<div class="form-group ${f.column}" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="checkbox">`;
                let rightHtml=`</div></div>`;
                formDisplay=leftHtml+option+rightHtml;
                $('#formDisplay').append(formDisplay);
            }else if(f.fieldType.split("-")[1]==="file"){
                formDisplay=`<div class="form-group ${f.column}" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><input type='${f.fieldType.split("-")[1]}'></div>`;
                $('#formDisplay').append(formDisplay);
            }else{
                formDisplay=`<div class="form-group ${f.column}" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><input type='${f.fieldType.split("-")[1]}' class="form-control" placeholder=${f.placeholder}></div>`;
                $('#formDisplay').append(formDisplay);
            }
        }else if(f.fieldType.split("-")[0]=="textarea"){
            formDisplay=`<div class="form-group ${f.column}" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><textarea class="form-control" rows="3" placeholder=${f.placeholder}></textarea></div>`;
            $('#formDisplay').append(formDisplay);
        }else if(f.fieldType.split("-")[0]=="select"){
            let option="";
            for(let o of f.fieldOptions){
                option+=`<option value=${o}> ${o}</option>`
            }
            let leftHtml=`<div class="form-group ${f.column}" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><select class="form-control">`;
            let rightHtml=`</select></div>`;
            formDisplay=leftHtml+option+rightHtml;
            $('#formDisplay').append(formDisplay);
        }else if(f.fieldType.split("-")[0]=="editor"){
            formDisplay=`<div class="form-group col-md-12" data-name="${f.fieldType}" data-fieldid="${f.fieldid}"><label class="control-label">${f.displayName}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><div id="editor${f.fieldid}" style="height:150px;border-bottom: 1px solid #ccc;overflow: hidden;"></div></div>`;
            $('#formDisplay').append(formDisplay);
            //创建富文本编辑器
            var E = window.wangEditor;
            var editor = new E('#editor'+f.fieldid);
            editor.customConfig.menus = [
                'head',
                'bold',
                'italic',
                'fontSize',  // 字号
                'fontName',
                'underline',
                'strikeThrough',  // 删除线
                'foreColor',  // 文字颜色
                'backColor',  // 背景颜色
                'link',  // 插入链接
                'justify',  // 对齐方式,
                'image',  // 插入图片
                'table',  // 表格
            ]
            editor.create();
        }
    }
}
//表单验证初始化
$(function () {
    $('form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            formName: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '表单名称不能为空'
                    }
                }
            },
            instruction: {
                validators: {
                    notEmpty: {
                        message: '表单介绍不能为空'
                    }
                }
            },
            displayName: {
                validators: {
                    notEmpty: {
                        message: '展示名称不能为空'
                    }
                }
            },
        }
    });
});
//左侧选择表单组件，并展示到预览样式里
function selectField(value,label) {
    let formDisplay='';
    //获取当前时间，作为该组件的fieldid
    const fieldid = Date.now();
    if(value.split("-")[0]=="input"){
        if(value.split("-")[1]==="radio"){
            formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="radio"><label class="radio-inline">
  <input type="radio" name="${value}" value="选项"> 选项
</label><label class="radio-inline">
  <input type="radio" name="${value}" value="选项"> 选项
</label></div></div>`;
            $('#formDisplay').append(formDisplay);
        }else if(value.split("-")[1]==="checkbox"){
            formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="checkbox"><label class="checkbox-inline"><input type="checkbox" value="选项"> 选项
</label><label class="checkbox-inline"><input type="checkbox" value="选项"> 选项</label></div></div>`;
            $('#formDisplay').append(formDisplay);
        }else if(value.split("-")[1]==="file"){
            formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><input type='${value.split("-")[1]}'></div>`;
            $('#formDisplay').append(formDisplay);
        }else{
            formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><input type='${value.split("-")[1]}' class="form-control"></div>`;
            $('#formDisplay').append(formDisplay);
        }
    }else if(value.split("-")[0]=="textarea"){
        formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><textarea class="form-control" rows="3"></textarea></div>`;
        $('#formDisplay').append(formDisplay);
    }else if(value.split("-")[0]=="select"){
        formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><select class="form-control" rows="3"><option>请选择</option><option value="选项">选项</option><option value="选项">选项</option></select></div>`;
        $('#formDisplay').append(formDisplay);
    }else if(value.split("-")[0]=="editor"){
        formDisplay=`<div class="form-group col-md-12" data-name="${value}" data-fieldid="${fieldid}"><label class="control-label">${label}</label><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button><div id="editor${fieldid}" style="height:150px;border-bottom: 1px solid #ccc;overflow: hidden;"></div></div>`;
        $('#formDisplay').append(formDisplay);
        //创建富文本编辑器
        var E = window.wangEditor;
        var editor = new E('#editor'+fieldid);
        editor.customConfig.menus = [
            'head',
            'bold',
            'italic',
            'fontSize',  // 字号
            'fontName',
            'underline',
            'strikeThrough',  // 删除线
            'foreColor',  // 文字颜色
            'backColor',  // 背景颜色
            'link',  // 插入链接
            'justify',  // 对齐方式
        ]
        editor.create();
    }
    //每当选择一个组件，就formFields存入数组中，存入的值都为默认值
    let tempObj={};
    tempObj.fieldid=fieldid;
    tempObj.fieldType=value;
    tempObj.displayName=label;
    // tempObj.fieldName=fieldName;
    tempObj.placeholder="";
    tempObj.isRequired="false";
    tempObj.column="col-md-12";
    tempObj.fieldOptions=[
        "选项",
        "选项"
    ];
    // tempObj.fieldOrder=index+1;
    formFields.push(tempObj);
    console.log(formFields);
}
//预览样式叉号点击关闭
$("#formDisplay").on('click',".close",function () {
    $(this).parent("div.form-group").remove();
    let fieldid=$(this).parent("div.form-group").data("fieldid");
    //遍历已经存起来的表单字段数组 ，并删除对应数据
    formFields.forEach(function(value,index){
        if (value.fieldid == fieldid) {
            formFields.remove(value);
        }
    })
})
//预览样式点击选中，右侧字段展示不同的属性
$("#formDisplay").on('click',".form-group",function () {
    $(this).addClass('active');
    $(this).siblings(".form-group").removeClass("active");
    if($(this).data('name')==='input-radio'||$(this).data('name')==='input-checkbox'||$(this).data('name')==='select-option'){
        $("#setField #addOption").show();
    }else{
        $("#setField #addOption").hide();
    }
    if($(this).data('name')==='input-date'||$(this).data('name')==='input-file'||$(this).data('name')==='input-radio'||$(this).data('name')==='input-checkbox'||$(this).data('name')==='select-option'){
        $("#setField #placeholderText").hide();
    }else{
        $("#setField #placeholderText").show();
    }
    //获取当前组件在数组中已经存的默认值并填入字段属性设置中
    let currentFieldId=$(this).data("fieldid");
    formFields.forEach(function(value,index){
        if (value.fieldid == currentFieldId) {
            for(let k in value){
                if(k=='column'){
                    $("#setField form input[name='column']").each(function(){
                        if($(this).val() == value[k]){
                            $(this).prop("checked", true);
                        }
                    });
                }else if(k=='isRequired'){
                    if(value[k]==true){
                        $("#setField form input[name='isRequired']").prop("checked", true)
                    }else if(value[k]==false){
                        $("#setField form input[name='isRequired']").prop("checked", false)
                    }
                }else if(k=='fieldOptions'){
                    $("#setField #addOption .control-group").empty();
                    for(let o of value[k]){
                        //清空，填入新的
                        let optionItem = `<div class="input-group">
      <input type="text" placeholder="输入选项内容" class="form-control" value=${o} /><span class="input-group-addon delete-option"><i class="glyphicon glyphicon-remove"></i></span>
    </div>
  </div>`
                        $("#setField #addOption .control-group").append(optionItem);
                    }
                }else{
                    $(`#setField form input[name=${k}]`).val(value[k]);
                }
            }
        }
    })
    //同步当前点击的预览表单组件名称和字段属性设置中展示名称
    $("#setField input[name='displayName']").val($(this).find("label.control-label").text());
    //点击预览表单组件时把组件fieldid加入到当前字段属性设置中的form name属性值中
    $("#setField form").attr("name",$(this).data("fieldid"));
    //把当前点击的组件的data-name属性赋值过去
    $("#setField form").attr("data-name",$(this).data("name"));
    //点击预览样式的时候遍历右侧选项，监听每个选项的值
    listenOption();
})
//点击预览样式的时候遍历右侧选项，监听每个选项的值
function listenOption() {
    $("#setField #addOption .control-group .input-group").each(function(index,value) {
        $(value).on('input propertychange','input',function() {
            let index = $(this).parent(".input-group").index();
            //输入选项的时候预览也同时删除
            let currentFieldId = $("#setField form").attr("name");
            if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "input-radio") {
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] .radio .radio-inline:eq(${index})`).html(`<input type="radio" name="input-radio" value=${$(this).val()}>${$(this).val()}`);
            } else if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "input-checkbox") {
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] .checkbox .checkbox-inline:eq(${index})`).html(`<input type="checkbox" name="input-checkbox" value=${$(this).val()}>${$(this).val()}`);
            } else if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "select-option") {
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] select option:eq(${index})`).val($(this).val());
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] select option:eq(${index})`).text($(this).val());
            }
        });
    });
}
//字段展示名称同步预览样式展示名称
$("#setField input[name='displayName']").on('input propertychange',function () {
    $("#formDisplay .form-group.active").find("label.control-label").text($(this).val());
})
//字段默认提示文字同步预览样式默认提示文字
$("#setField input[name='placeholder']").on('input propertychange',function () {
    if($("#formDisplay .form-group.active").data('name')==='textarea-textarea'){
        $("#formDisplay .form-group.active").find("textarea").attr('placeholder',$(this).val());
    }else{
        $("#formDisplay .form-group.active").find("input").attr('placeholder',$(this).val());
    }
})
//字段展示宽度同步预览样式宽度
$("#setField input[name='column']").on('input propertychange',function () {
    $("#formDisplay .form-group.active").attr('class','form-group active '+$(this).val());
})
//页面加载时遍历右侧选项，监听每个选项的值
listenOption();
//添加选项
function addOption() {
    let optionItem=`<div class="input-group">
          <input type="text" placeholder="输入选项内容" name="fieldOptions" class="form-control" value="选项"/><span class="input-group-addon delete-option"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
      </div>`
    $("#setField #addOption .control-group").append(optionItem);
    //添加选项的时候预览也添加一个
    let currentFieldId=$("#setField form").attr("name");
    if($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name")==="input-radio"){
        $("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).find(".radio").append(`<label class="radio-inline">
  <input type="radio" name="input-radio" value="选项"> 选项
</label>`)
    }else if($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name")==="input-checkbox"){
        $("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).find(".checkbox").append(`<label class="checkbox-inline"><input type="checkbox" value="选项"> 选项
</label>`)
    }else if($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name")==="select-option"){
        $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] select`).append(`<option value="选项">选项</option>`)
    }
    //点击预览样式的时候遍历右侧选项，监听每个选项的值
    $("#setField #addOption .control-group .input-group").each(function(index,value) {
        $(value).on('input propertychange','input',function() {
            let index = $(this).parent(".input-group").index();
            //输入选项的时候预览也同时删除
            let currentFieldId = $("#setField form").attr("name");
            if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "input-radio") {
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] .radio .radio-inline:eq(${index})`).html(`<input type="radio" name="input-radio" value=${$(this).val()}>${$(this).val()}`);
            } else if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "input-checkbox") {
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] .checkbox .checkbox-inline:eq(${index})`).html(`<input type="checkbox" name="input-checkbox" value=${$(this).val()}>${$(this).val()}`);
            } else if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "select-option") {
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] select option:eq(${index})`).val($(this).val());
                $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] select option:eq(${index})`).text($(this).val());
            }
        });
    });
}
//删除选项
$("#optionGroup").on('click','.input-group .delete-option',function () {
    let index = $(this).parents(".input-group").index();
    //删除选项的时候预览也同时删除
    let currentFieldId = $("#setField form").attr("name");
    if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "input-radio") {
        $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] .radio .radio-inline:eq(${index})`).remove();
    } else if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "input-checkbox") {
        $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] .checkbox .checkbox-inline:eq(${index})`).remove();
    } else if ($("#formDisplay").find(`div[data-fieldid=${currentFieldId}]`).data("name") === "select-option") {
        $("#formDisplay").find(`div[data-fieldid=${currentFieldId}] select option:eq(${index})`).remove();
    }
    $(this).parents(".input-group").remove();
    saveField();
})
//表单字段值数组
function saveField() {
    $("#setField form").data("bootstrapValidator").validate();
    let validFlag = $("#setField form").data("bootstrapValidator").isValid();
    if(validFlag){
        let displayName=$("input[name='displayName']").val();
        // let fieldName=$("input[name='fieldName']").val();
        let fieldid=$("#setField form").attr("name");
        let placeholder=$("input[name='placeholder']").val();
        let isRequired=false;
        if($("input[name='isRequired']").is(':checked')) {
            isRequired=true;
        }else{
            isRequired=false;
        }
        let column=$("input[name='column']:checked").val();
        let fieldOptions=[];
        $("#addOption .input-group").each(function () {
            if($(this).find('input').val()){
                fieldOptions.push($(this).find('input').val());
            }
        })
        //图片 or 文件  限制图片/文件大小
        // maxSize:String, 文字长度
        let tempObj={};
        tempObj.fieldid=fieldid;
        tempObj.fieldType=$("#setField form").attr("data-name");
        tempObj.displayName=displayName;
        // tempObj.fieldName=fieldName;
        tempObj.placeholder=placeholder;
        tempObj.isRequired=isRequired;
        tempObj.column=column;
        tempObj.fieldOptions=fieldOptions;
        // tempObj.fieldOrder=index+1;
        //遍历已经存起来的表单字段数组 ，排重
        if (formFields.length > 0) {
            let ifdoadd = true;
            for (let index = 0; index < formFields.length; index++) {
                let value = formFields[index];
                if (value.fieldid == fieldid) {
                    formFields[index] = tempObj
                    ifdoadd = false;
                    $("#saveBtn").before(`<div class="alert alert-success">字段修改成功</div>`)
                    setTimeout(function () {
                        $("#saveBtn").prev(".alert-success").remove();
                    }, 1000)
                }
            }
            if (ifdoadd) {
                //push到表单字段数组中
                formFields.push(tempObj);
                $("#saveBtn").before(`<div class="alert alert-success">字段属性保存成功</div>`)
                setTimeout(function () {
                    $("#saveBtn").prev(".alert-success").remove();
                }, 1000)
            }
        } else {
            formFields.push(tempObj);
            $("#saveBtn").before(`<div class="alert alert-success">字段属性保存成功</div>`)
            setTimeout(function () {
                $("#saveBtn").prev(".alert-success").remove();
            }, 1000)
        }
        console.log(formFields);
    }
}
$("#setField form").change(function(){
    saveField();
})
//创建表单模版
function createFormModel(){
    $("#createFormModel").data("bootstrapValidator").validate();
    let validFlag = $("#createFormModel").data("bootstrapValidator").isValid();
    if(validFlag){
        let formModelData={
            _csrf:_csrf,
            formName:$("input[name='formName']").val(),
            instruction:$("textarea[name='instruction']").val(),
            createUserName:getCookie('username'),
            createUserId:getCookie('userId'),
            formFields:formFields
        };
        $.ajax({
            type : "POST",
            url : "/api/formModel/create",
            data : formModelData,
            success : function(result) {
                $('#alert-success').show();
                setTimeout(()=>{
                    window.location.href="/formModelList"
                },1000)
            },
            error:function (result) {
                $('#alert-alert').show();
                setTimeout(()=>{
                    $('#alert-alert').hide();
                },3000)
            }
        });
    }
}
//更新表单模版
function updateFormModel(){
    $("#updateFormModel").data("bootstrapValidator").validate();
    let validFlag = $("#updateFormModel").data("bootstrapValidator").isValid();
    if(validFlag){
        let id=JSON.parse($("#formModelInfo").val())._id;
        $.ajax({
            type : "POST",
            url : "/api/formModel/update",
            data : {
                id:id,
                status:0
            },
            success : function(result) {
                console.log(result)
            },
            error:function (result) {
                console.log(result)
            }
        });
        let formModelData={
            _csrf:_csrf,
            formName:$("input[name='formName']").val(),
            instruction:$("textarea[name='instruction']").val(),
            createUserName:$("input[name='createUserName']").val(),
            createUserId:$("input[name='createUserId']").val(),
            updateUserName:getCookie('username'),
            updateUserId:getCookie('userId'),
            formFields:formFields,
            status:1
        };
        $.ajax({
            type : "POST",
            url : "/api/formModel/updateIteration",
            data : formModelData,
            success : function(result) {
                $('#alert-success').show();
                setTimeout(()=>{
                    window.location.href="/formModelList"
                },1000)
            },
            error:function (result) {
                $('#alert-alert').show();
                setTimeout(()=>{
                    $('#alert-alert').hide();
                },3000)
            }
        });
    }
}
