//定义表单模版和表格模版空数组
let formModels=[];//定义项目的formModels字段
let tableModels=[];//定义项目的tableModels字段
let modelData=JSON.parse($("input[name='modelData']").val());
let projectModel=JSON.parse($("input[name='projectModel']").val());
let statusList=JSON.parse($("input[name='projectModel']").val()).statusList;//定义流程状态数组
let projectId=JSON.parse($("input[name='project']").val())._id;//获取项目id
let projectData={};//定义传入update接口的整个对象
let data=[];//定义传入的数据数组，传入整个表单数据，table单条数据不在这里插入，在插入的时候直接存数据库
//表单验证初始化
$(function () {
    $('form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            projectName: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '项目名称不能为空'
                    }
                }
            },
            instruction: {
                validators: {
                    notEmpty: {
                        message: '项目介绍不能为空'
                    }
                }
            },
            projectModel: {
                validators: {
                    notEmpty: {
                            message: '请选择项目模板'
                    }
                }
            }
        }
    });
});

/********** 编辑项目页面 ************/
//编辑项目状态，插入项目状态select列表,删除默认的项目状态input
function editStatus(e) {
    $(e).hide();
    let curStatusName=$($("#status input[name='statusName']")).val()
    $("#status").data("status","select");
    $("#status input").remove();
    $("#status").append(`<select id="statusSelect" onchange="selectStatus(value)" name="statusId" class="form-control"><option value="">请选择流程状态</option></select>`);
    $("#statusSelect").empty();
    for (let i = 0; i < statusList.length; i++) {
        if(statusList[i].statusName===curStatusName){
            $("#statusSelect").append(`<option value=${statusList[i]._id} selected="selected">${statusList[i].statusName}</option>`);
        }else{
            $("#statusSelect").append(`<option value=${statusList[i]._id}>${statusList[i].statusName}</option>`);
        }
    }
}
//选择流程/状态，联动表单列表和表格列表
function selectStatus(value) {
    //清空表单表格模版列表
    $("#accordion1").empty();
    $("#accordion2").empty();
    //联动表单和表格模版
    for(let v of statusList){
        if (value === v._id) {
            for(let form of v.formModels){
                $("#accordion1").append(`<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#${form._id}" data-toggle="collapse">${form.formName}</a></h4>
  </div>
  <div class="panel-collapse collapse" id="${form._id}">
    <div class="panel-body"></div>
  </div>
</div>`);
            }
            for(let table of v.tableModels){
                $("#accordion2").append(`<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#${table._id}" data-toggle="collapse">${table.tableName}</a></h4>
  </div>
  <div class="panel-collapse collapse" id="${table._id}">
    <div class="panel-body"></div>
  </div>
</div>`);
            }
        }
    }
    renderModel();
}
//默认
function renderModel() {
    //渲染表单模板
    $("#accordion1 .panel-default").each(function (index,value) {
        const _this=$(this);
        let id=_this.find(".panel-collapse").attr("id");
        _this.find(".panel-body").append(`<form></form>`);
        $.ajax({
            type : "GET",
            url : "/api/project/renderFormModel",
            data : {
                id:id
            },
            success : function(result) {
                let formData=''
                if(modelData[id].length>0){
                     formData=JSON.parse(modelData[id][0].data);
                }
                $(result.formFields).each(function (index,value) {
                    let formItem="";
                    let v="";
                    if(formData[value.displayName]){
                        v=formData[value.displayName];
                    }else{
                        v="";
                    }
                    let placeholderStr=value.placeholder?value.placeholder:"";
                    if(value.fieldType.split('-')[0]==='input'){
                        if(value.fieldType.split('-')[1]==='radio'){//单选按钮
                            let formItemBoxLeft=`<div class="form-group ${value.column}"><label>${value.displayName}</label>`;
                            let formItemBoxRight=`</div>`;
                            let formBoxItem="";
                            $(value.fieldOptions).each(function (index,item){
                                formBoxItem+=`<div class=${value.fieldType.split('-')[1]}><label><input name=${value.displayName} type=${value.fieldType.split('-')[1]} value=${item} />${item}</label></div>`
                            });
                            formItem=formItemBoxLeft+formBoxItem+formItemBoxRight;
                            _this.find(".panel-body form").append(formItem);
                        }else if(value.fieldType.split('-')[1]==='checkbox'){//复选框
                            let formItemBoxLeft=`<div class="form-group ${value.column}"><label>${value.displayName}</label>`;
                            let formItemBoxRight=`</div>`;
                            let formBoxItem="";
                            $(value.fieldOptions).each(function (index,item){
                                formBoxItem+=`<div class=${value.fieldType.split('-')[1]}><label><input name="${value.displayName}[]" type=${value.fieldType.split('-')[1]} value=${item} />${item}</label></div>`
                            });
                            formItem=formItemBoxLeft+formBoxItem+formItemBoxRight;
                            _this.find(".panel-body form").append(formItem);
                        }else if(value.fieldType.split('-')[1]==='file'){//上传文件
                            if(value.fieldType.split('-')[2]==='file'){
                                let fileId=Date.now();
                                formItem=`<div class="form-group ${value.column}" id='file${fileId}' data-name="${value.displayName}" data-type="${value.fieldType}"><label>${value.displayName}</label><div style="position: absolute;"><button style="margin-right:10px;" type="button" class="btn btn-default btn-sm" >上传文件</button></div><input style="opacity: 0;width:100px;margin-bottom:20px;" name=${value.displayName} type=${value.fieldType.split('-')[1]} multiple onchange="uploadFile(this)" /></div>`
                                _this.find(".panel-body form").append(formItem);
                                if(v.length>0){
                                    for(let f of v){
                                        console.log(f)
                                        let filename=(f.split("/")[f.split("/").length-1]);
                                        let fileext=(f.split("/")[f.split("/").length-1]).split(".")[1];
                                        let preUrl='';
                                        if(fileext==="doc"||fileext==='ppt'||fileext==='xls'||fileext==="docx"||fileext==='pptx'||fileext==='xlsx'){
                                            preUrl="https://view.officeapps.live.com/op/view.aspx?src="+encodeURIComponent(f)
                                        }else{
                                            preUrl=f
                                        }
                                        let fileItem=`<div class="alert alert-success" data-url="${f}">${filename} 已上传 <a href="/pre?path=${preUrl}" target="_blank">查看</a><a href="${preUrl}" target="_blank">下载</a><button type="button" class="close" aria-label="Close" onclick="removeFile(this)"><span aria-hidden="true">&times;</span></button></div>`
                                        $("#file"+fileId).append(fileItem);
                                    }
                                }
                            }else if(value.fieldType.split('-')[2]==='image'){
                                let fileId=Date.now();
                                formItem = `<div class="form-group ${value.column}" id='image${fileId}' data-name="${value.displayName}" data-type="${value.fieldType}">
                                            <label>${value.displayName}</label>
                                            <div class="imgPreview">
                                                <div class="imgBoxAdd">
                                                    <div class="panel updatepanel">
                                                        <div class="addbox">+</div>
                                                        <input type="file" id="image" multiple onchange="uploadFile(this)" />
                                                    </div>
                                                </div>
                                            </div>`
                                _this.find(".panel-body form").append(formItem);
                                if(v.length>0){
                                    for(let f of v){
                                        let imgItem=`<div class="imgBox" data-url="${f}">
									<div class="panel panel-info">
										<div class="panel-heading" onclick="removeImg(this)">
											<i class="fa fa-times"></i>
										</div>
										<div class="panel-body" style="text-align: center;">
											<div class="row">
												<div class="col-sm-12 col-md-12">
													<img class="updateimg img-responsive" src=${f} />
												</div>
											</div>
										</div>
									</div>
								</div>`
                                        $("#image"+fileId).find(".imgPreview").prepend(imgItem);
                                    }
                                }
                            }
                        }else{//文本框
                            formItem=`<div class="form-group ${value.column}"><label>${value.displayName}</label><input name=${value.displayName} type=${value.fieldType.split('-')[1]} placeholder="${placeholderStr}" class="form-control" value=${v}></div>`
                            _this.find(".panel-body form").append(formItem);
                        }
                    }else if(value.fieldType.split('-')[0]==='select'){//下拉框
                        let formItemBoxLeft=`<div class="form-group ${value.column}"><label>${value.displayName}</label><select name=${value.displayName} class="form-control">`;
                        let formItemBoxRight=`</select></div>`;
                        let formBoxItem="";
                        $(value.fieldOptions).each(function (index,item){
                            formBoxItem+=`<option value=${item}>${item}</option>`
                        });
                        formItem=formItemBoxLeft+formBoxItem+formItemBoxRight;
                        _this.find(".panel-body form").append(formItem);
                    }else if(value.fieldType.split('-')[0]==='textarea'){//多行文本
                        formItem=`<div class="form-group ${value.column}"><label>${value.displayName}</label><textarea name=${value.displayName} rows="3"  class="form-control" placeholder='${placeholderStr}'>${v}</textarea></div>`
                        _this.find(".panel-body form").append(formItem);
                    }else if(value.fieldType.split('-')[0]==='editor'){//富文本编辑器
                        formItem=`<div class="form-group ${value.column}" data-name="${value.displayName}" data-type="${value.fieldType}"><label>${value.displayName}</label><div id="editor${value.fieldid}">${v}</div></div>`
                        _this.find(".panel-body form").append(formItem);
                        //创建富文本编辑器
                        var E = window.wangEditor;
                        var editor = new E('#editor'+value.fieldid);
                        editor.customConfig.menus = [
                            'head',
                            'bold',
                            'italic',
                            'fontSize',  // 字号
                            'fontName',
                            'underline',
                            'strikeThrough',  // 删除线
                            'foreColor',  // 文字颜色
                            'backColor',  // 背景颜色
                            'link',  // 插入链接
                            'justify',  // 对齐方式
                            'image',  // 插入图片
                            'table',  // 表格
                        ]
                        editor.customConfig.debug = true;
                        editor.customConfig.uploadImgServer = '/api/project/uploadFile';
                        // 隐藏“网络图片”tab
                        editor.customConfig.showLinkImg = false;
                        editor.customConfig.uploadFileName = 'file'
                        editor.create();
                        // editor.txt.html(v);
                    }
                })
                let arr = Object.keys(formData);
                $.each($("#accordion1 .panel-body"),function (i1,v1) {
                    $.each($(v1).find("input"),function (i2,v2) {
                        //单选按钮默认选中
                        if($(this).prop("type")==='radio'){
                            if(arr.length>0&&formData[$(this).prop("name")][0]===$(this).val()){
                                $(this).prop("checked","checked");
                            }
                        }
                        //多选框默认选中
                        if($(this).prop("type")==='checkbox'&&arr.length>0){
                            for(let c of formData[$(this).prop("name")]){
                                if(c===$(this).val()){
                                    $(this).prop("checked","checked");
                                }
                            }
                        }
                    })
                })
                $.each($("#accordion1 .panel-body form select"),function (i,v) {
                    for(let o of $(v).find("option")){
                        if(formData.length>0&&formData[$(v).parent("select").prop("name")].toString()===$(o).val()){
                            $(o).prop("selected","selected");
                        }
                    }
                })
            },
            error:function (result) {
                console.error(result)
            }
        });
    })
    //渲染表格模板
    $("#accordion2 .panel-default").each(function (index,value) {
        const _this=$(this);
        let id=_this.find(".panel-collapse").attr("id");
        _this.find(".panel-body").append(`<button type="button" onclick="addRow('${id}')" class="btn btn-default" style="margin-bottom:20px;">插入一行</button><table class="table table-striped table-bordered table-hover" id="table${id}"></table>`);
        let columns=[];
        let data=[];
        for(let t of modelData[id]){
            let localdata  = JSON.parse(t.data)
            localdata['_id'] = t._id;
            localdata['tool'] = `<a style="margin-bottom:5px;" onclick="removeOne(this,'${t._id}','projectData')" class="btn btn-default"> 删除</a>`;
            data.push(localdata)
        }
        $.ajax({
            type : "GET",
            url : "/api/project/renderTableModel",
            data : {
                id:id
            },
            success : function(result) {
                $(result.table).each(function (index,value) {
                    let tempObj={};
                    tempObj.data=value.fieldName;
                    tempObj.title=value.headerName;
                    columns.push(tempObj)
                })
                let idObj={};
                idObj.data='_id';
                idObj.title='id';
                idObj.visible=false;
                let toolsObj={};
                toolsObj.data='tool';
                toolsObj.title='操作';
                columns.push(idObj)
                columns.push(toolsObj)
                console.log(columns)
                let t = $("#table"+id).DataTable({
                    searching: false,
                    ordering:  false,
                    lengthChange:false,
                    data:data,
                    columns: columns
                });
            },
            error:function (result) {
                console.error(result)
            }
        });
    })
}
renderModel();
//表格插入一行
function addRow(id) {
    $.ajax({
        type : "GET",
        url : "/api/project/renderTableModel",
        data : {
            id:id
        },
        success : function(result) {
            $("#addRow .modal-body form").empty();
            $(result.table).each(function (index,value) {
                let label=value.headerName;
                let fieldType=value.fieldType;
                let fieldName=value.fieldName;
                if(fieldType==='text'){
                    $("#addRow .modal-body form").append(`<div class="form-group"><label>${label}</label>
  <input type=${fieldType} placeholder="请填写${label}" class="form-control" name=${fieldName}
</div>`)
                }else if(fieldType==='file'){
                    $("#addRow .modal-body form").append(`<div class="form-group"><label>${label}</label><div style="position: absolute;"><button style="margin-right:10px;" type="button" class="btn btn-default btn-sm" >上传文件</button><span class="uploadProgress"></span><span class="fileDelete glyphicon glyphicon-remove" style="display: none" onclick="fileDelete(this)"></span></div><input data-url="" style="opacity: 0;width:100px;" name=${fieldName} type=${fieldType} onchange="uploadFile(this)" /><p class="fileName" style="display: none"><a href="" target="_blank">查看</a><a href="" class="fileDownload" target="_blank">下载</a></p></div>`)
                }
            })
        },
        error:function (result) {
            console.error(result)
        }
    });
    $("#addRow .modal-footer").empty();
    $("#addRow .modal-footer").append(`<button type="button" data-dismiss="modal" class="btn btn-default cancel">取消</button><button type="button" onclick="confirmRow('${id}')" class="btn btn-primary delete">确认</button>`)
    $("#addRow").modal();
}
// 确认插入一行,直接把数据存到数据库中，提交的时候不传入
function confirmRow(id) {
    let dataObj = {};
    let m = {};
    $.each($("#addRow form").serializeArray(), function(i, field){
        m[field.name]=field.value;
    });
    m['tool'] = `<a style="margin-bottom:5px;" onclick="removeOne(this,'${m._id}','projectData')" class="btn btn-default"> 删除</a>`;
    //获取file文件地址
    $.each($("#addRow form input"), function(i, v){
        if($(v).attr("type")==="file"){
            let name=$(v).attr("name");
            let value=$(v).attr("data-url");
            m[name]=value
        }
    });
    dataObj.projectId=projectId;
    dataObj.projectSubmoduleId=id;
    dataObj.data=JSON.stringify(m);
    dataObj.createUserId=getCookie('userId');
    dataObj.createUserName=getCookie('username');
    $.ajax({
        type: "POST",
        url: "/api/projectData/add",
        data: dataObj,
        success: function (result) {
            console.log(result)
        },
        error: function (result) {
            console.error(result)
        }
    });
    let t=$('#table'+id).DataTable();
    t.row.add(m).draw( false );
    $("#addRow").modal('hide');
}
//通用删除单条数据
function removeOne(e,id,name) {
    $("#myModal .modal-body p").html("确认删除该条数据吗？");
    $("#myModal"). modal();
    $("#delete").click(function () {
        $.ajax({
            type : "GET",
            url : "/api/"+name+"/delete",
            data : {
                id:id
            },
            success : function(result) {
                $("#myModal"). modal('hide');
                let t=$('#table'+id).DataTable();
                // t.row($(e).parents('tr')).remove().draw();
                $(e).parents("tr").remove();
            },
            error:function (result) {
                $("#myModal"). modal('hide');
                console.log(result)
            }
        });
    })
}
//更新项目页面提交
function updateProject() {
    $("#updateProject").data("bootstrapValidator").validate();
    let validFlag = $("#updateProject").data("bootstrapValidator").isValid();
    if(validFlag){
        //遍历当前表单模版
        formModels=[];
        $("#accordion1 .panel-default").each(function (i,v) {
            //遍历当前表单模版，把id和名称存入数组，传入后端，存到project表的formModels中
            let obj={};
            obj._id=$(v).find(".panel-collapse").attr("id");
            obj.formName=$(v).find(".panel-title a").text();
            formModels.push(obj);
            //获取当前表单模版填写的信息
            let m = {};
            $.each($(v).find(".panel-body form").serializeArray(), function(i, field){
                if(!m.hasOwnProperty(field.name)){
                    m[field.name]=[];
                }
                m[field.name].push(field.value)
            });
            //input file存储
            $.each($(v).find(".panel-body .form-group"), function(i1, v1){
                let type=$(v1).attr("data-type");
                if(type==="input-file-image"){
                    let name=$(v1).attr("data-name");
                    m[name]=[];
                    $.each($(v1).find(".imgPreview .imgBox"),function (i2,v2) {
                        m[name].push($(v2).attr("data-url"));
                    })
                }else if(type==="input-file-file"){
                    let name=$(v1).attr("data-name");
                    m[name]=[];
                    $.each($(v1).find(".alert"),function (i2,v2) {
                        m[name].push($(v2).attr("data-url"));
                    })
                }
            });
            //富文本存储
            $.each($(v).find(".panel-body .form-group"), function(i1, v1){
                let type=$(v1).attr("data-type");
                if(type==="editor-editor"){
                    let name=$(v1).attr("data-name");
                    m[name]=$(v1).find(".w-e-text").html();
                    console.log($(v1).find(".w-e-text").html())
                }
            });
            //转为json字符串
            let formData=JSON.stringify(m);
            let dataObj={};
            dataObj.projectId=projectId;
            dataObj.projectSubmoduleId=obj._id;
            dataObj.data=formData;
            //遍历当前项目存的数据表
            let arr = Object.keys(modelData);
            if(arr.length>0){
                for(let key in modelData){
                    if(obj._id===key&&modelData[key].length>0){
                        dataObj.createAt = modelData[key][0].createAt;
                        dataObj.updateAt = new Date;
                        dataObj.updateUserId=getCookie('userId');
                        dataObj.updateUserName=getCookie('username');
                    }else{
                        dataObj.createUserId=getCookie('userId');
                        dataObj.createUserName=getCookie('username');
                    }
                }
            }else{
                dataObj.createUserId=getCookie('userId');
                dataObj.createUserName=getCookie('username');
            }
            data.push(dataObj);
        })
        //遍历当前表格模版
        tableModels=[];
        $("#accordion2 .panel-default").each(function (i,v) {
            //把id和名称存入数组，传入后端，存到project表的tableModels中
            let obj={};
            obj._id=$(v).find(".panel-collapse").attr("id");
            obj.tableName=$(v).find(".panel-title a").text();
            tableModels.push(obj);
        })
        //获取当前项目信息
        let m = {};
        $.each($("#updateProject").serializeArray(), function(i, field){
            m[field.name]=field.value;
        });
        projectData.projectName=m.projectName;
        projectData.instruction=m.instruction;
        projectData._id=projectId;
        projectData.formModels=formModels;
        projectData.tableModels=tableModels;
        if($("#status").data("status")==='select'){
            projectData.statusName=$("#updateProject select[name='statusId'] option:selected").text();
            projectData.statusId=$("#updateProject select[name='statusId'] option:selected").val();
        }else{
            projectData.statusName=m.statusName;
            projectData.statusId=m.statusId;
        }
        projectData.data=data;
        projectData.projectModelId=m.projectModelId;
        projectData.projectModelName=m.projectModelName;
        projectData.updateUserName=getCookie('username');
        projectData.updateUserId=getCookie('userId');
        projectData.createAt=JSON.parse(m.project).createAt;
        projectData.updateAt=new Date;
        console.log(projectData)
        $.ajax({
            type: "POST",
            url: "/api/project/update",
            data: projectData,
            success: function (result) {
                console.log(result)
                // if(result.result===true){
                    $('#alert-success').show();
                    setTimeout(()=>{
                        $('#alert-success').hide();
                        window.location.href="/projectList"
                    },1000)
                /*}else{
                    $('#alert-danger').show();
                    setTimeout(()=>{
                        $('#alert-danger').hide();
                        window.location.href="/projectList"
                    },1000)
                }*/
            },
            error: function (result) {
                console.error(result)
                $('#alert-danger').show();
                setTimeout(()=>{
                    $('#alert-danger').hide();
                    window.location.href="/projectList"
                },1000)
            }
        });
    }
}
//把datatable警告置空
$.fn.dataTable.ext.errMode = 'none';
function fileDelete(e){
    console.log("Aaa")
    $(e).parent("div").next("input").attr("data-url","");
    $(e).parent("div").next("input").next(".fileName").hide();
    $(e).prev(".uploadProgress").text("");
    $(e).hide();
}
function uploadFile(e){
    console.log($(e).val());
    let files = e.files;
    const fileLen = files.length;
    if (fileLen) {
        let fd = new FormData();
        for (let i = 0; i < fileLen; i++) {
            const file = files[i];
            fd.append('file',files[i]);
            let date = new Date();
            let path="project/"+getCookie('userId')+"/"+date.getFullYear()+(date.getMonth()+1)+date.getDate()+date.getHours()+date.getMinutes()+date.getSeconds()+date.getMilliseconds()+ "." + file.name.split('.').pop();
            // let size=file.size;
            // console.log(path)
            // console.log(size)
            /*if (/^image\/png$|jpeg$/.test(file.type)) {
                let src = window.URL.createObjectURL(file)||window.webkitURL.createObjectURL(file);
                let imgItem=`<div class="imgBox">
									<div class="panel panel-info">
										<div class="panel-heading" onclick="removeImg(this)">
											<i class="fa fa-times"></i>
										</div>
										<div class="panel-body" style="text-align: center;">
											<div class="row">
												<div class="col-sm-12 col-md-12">
													<img class="updateimg img-responsive" src=${src} />
												</div>
											</div>
										</div>
									</div>
								</div>`
                $(e).parents(".imgPreview").prepend(imgItem);
            }*/
        }
        $.ajax({
            type: "POST",
            url: "/api/project/uploadFile",
            cache: false,
            dataType: "text",
            data: fd,
            //必须false才会避开jQuery对 formdata 的默认处理
            // XMLHttpRequest会对 formdata 进行正确的处理
            processData: false,
            //必须false才会自动加上正确的Content-Type
            contentType: false,
            xhrFields: {
                withCredentials: true
            },
            success: function (result) {
                for(let f of JSON.parse(result).data){
                    let filename=(f.split("/")[f.split("/").length-1]);
                    let fileext=(f.split("/")[f.split("/").length-1]).split(".")[1];
                    let preUrl='';
                    if(fileext==="doc"||fileext==='ppt'||fileext==='xls'||fileext==="docx"||fileext==='pptx'||fileext==='xlsx'){
                        preUrl="https://view.officeapps.live.com/op/view.aspx?src="+encodeURIComponent(f)
                    }else{
                        preUrl=f
                    }
                    if (/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(f)) {
                        let imgItem=`<div class="imgBox" data-url="${f}">
									<div class="panel panel-info">
										<div class="panel-heading" onclick="removeImg(this)">
											<i class="fa fa-times"></i>
										</div>
										<div class="panel-body" style="text-align: center;">
											<div class="row">
												<div class="col-sm-12 col-md-12">
													<img class="updateimg img-responsive" src=${f} />
												</div>
											</div>
										</div>
									</div>
								</div>`
                        $(e).parents(".imgPreview").prepend(imgItem);
                    }else{
                        let fileItem=`<div class="alert alert-success" data-url="${f}">${filename} 上传成功！ <a href="/pre?path=${preUrl}" target="_blank">查看</a><a href="${preUrl}" target="_blank">下载</a><button type="button" class="close" aria-label="Close" onclick="removeFile(this)"><span aria-hidden="true">&times;</span></button></div>`
                        $(e).parent("div.form-group").append(fileItem);
                    }
                }
                /*$(e).prev("div").find(".fileDelete").click(function () {
                 $.ajax({
                 type: "POST",
                 url: "/api/project/deleteFile",
                 data: {
                 url:result,
                 path:result.split("/")[result.split("/").length-1]
                 },
                 success: function (result) {
                 console.log("删除成功")
                 console.log(result)
                 },
                 error: function (result) {
                 console.log("删除失败")
                 console.error(result)
                 }
                 });
                 })*/
                /*$(e).next("p").find(".fileDownload").click(function () {
                 console.log("点击下载")
                 $.ajax({
                 type: "POST",
                 url: "/api/project/downloadFile",
                 data: {
                 url:result,
                 path:result.split("/")[result.split("/").length-1]
                 },
                 success: function (result) {
                 console.log("下载成功")
                 console.log(result)
                 },
                 error: function (result) {
                 console.log("下载失败")
                 console.error(result)
                 }
                 });
                 })*/
            },
            xhr: function() { //在jquery函数中直接使用ajax的XMLHttpRequest对象
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                        console.log("提交进度:" + percentComplete.toString() + '%'); //在控制台打印上传进度
                    }
                }, false);
                return xhr;
            },
            error: function (result) {
                let fileItem=`<div class="alert alert-danger">${filename} 上传失败，请重试！ <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`
                $(e).parent("div.form-group").append(fileItem);
            }
        });
    }
}
function removeImg(e) {
    $(e).parents(".imgBox").remove();
}
function removeFile(e) {
    $(e).parent("div.alert").remove();
}