//cookie 封装 获取 cookie
function getCookie(name) {
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}
//删除cookie
function delCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(cval!=null)
        document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}
//设置cookie 以及过期时间
function setCookie(name,value,time){
    var strsec = getsec(time);
    var exp = new Date();
    exp.setTime(exp.getTime() + strsec*1);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
//获取设置过期时间
function getsec(str){
    var str1=str.substring(1,str.length)*1;
    var str2=str.substring(0,1);
    if (str2=="s") {
        return str1*1000;
    } else if (str2=="h"){
        return str1*60*60*1000;
    } else if (str2=="d"){
        return str1*24*60*60*1000;
    }
}
//通用删除单条数据
function removeOne(e,id,name) {
    $("#myModal .modal-body p").html("确认删除该条数据吗？");
    $("#myModal"). modal();
    $("#delete").click(function () {
        $.ajax({
            type : "GET",
            url : "/api/"+name+"/delete",
            data : {
                id:id
            },
            success : function(result) {
                $("#myModal"). modal('hide');
                $(e).parents("tr").remove();
            },
            error:function (result) {
                $("#myModal"). modal('hide');
                console.log(result)
            }
        });
    })
}
//通用选中全部
function checkAll(id) {
    let checkStatus=$(id).prop("checked");
    let checkboxArr=$("tbody tr td:first-child input[type='checkbox']");
    checkboxArr.each(function (index,value) {
        $(value).prop("checked",checkStatus);
    })
}
//通用批量删除
function removeMany(name) {
    $("#myModal .modal-body p").html("确认删除选中数据吗？");
    $("#myModal").modal();
    let idArr=[];
    let checkboxArr=$("tbody tr td:first-child input[type='checkbox']");
    checkboxArr.each(function (index,value) {
        if($(value).prop("checked")){
            idArr.push($(value).val())
        }
    })
    $("#delete").click(function () {
        $.ajax({
            type : "POST",
            url : "/api/"+name+"/deleteMany",
            data :{
                idArr:idArr
            },
            success : function(result) {
                $("#myModal"). modal('hide');
                checkboxArr.each(function (index,value) {
                    if($(value).prop("checked")){
                        $(this).parents("tr").remove();
                    }
                })
            },
            error:function (result) {
                $("#myModal"). modal('hide');
                console.log(result)
            }
        });
    })
}
//删除数组中一个元素
Array.prototype.indexOf = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};