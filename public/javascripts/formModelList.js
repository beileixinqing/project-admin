$(function () {
    let totalPages=0;
    let currentPage=1;
    let numberOfPages=8;
    $.ajax({
        url: '/api/formModel/list',
        type: 'POST',
        data: {page: 1, size: 10},
        success: function (result) {
            $('#formModelList tbody').empty();
            let formListItem="";
            if(result.total<=0){
                formListItem+=`<tr>
                    <td colspan="8" style="text-align: center">暂无数据</td>
                    </tr>`;
                $('#formModelList tbody').append(formListItem);
                return false;
            }
            for(let l of result.list){
                let createUserName=l.createUserName?l.createUserName:"";
                let updateUserName=l.updateUserName?l.updateUserName:"";
                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                formListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.formName}</td>
                    <td>${l.instruction}</td>
                    <td>${createUserName}</td>
                    <td>${createAt}</td>
                    <td>${updateUserName}</td>
                    <td>${updateAt}</td>
                    <td>
                        <a href="/updateformModel?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <a style="margin-bottom:5px;margin-right:10px;" onclick="preview('${l._id}','formModel')" class="btn btn-info"> 查看</a>
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','formModel')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
            }
            $('#formModelList tbody').append(formListItem);
            totalPages=result.totalPages;
            currentPage=result.currentPage;
            // $('.totalPages').text(result.totalPages)
            $('#pageLimit').bootstrapPaginator({
                currentPage: currentPage,
                totalPages: totalPages,
                size:'normal',
                bootstrapMajorVersion: 3,
                alignment:"left",
                numberOfPages:numberOfPages,
                itemTexts: function (type, page, current) {
                    switch (type) {
                        case "first": return "首页";
                        case "prev": return "上一页";
                        case "next": return "下一页";
                        case "last": return "末页";
                        case "page": return page;
                    }//默认显示的是第一页。
                },
                onPageClicked: function (event, originalEvent, type, page) {//给每个页眉绑定一个事件，其实就是ajax请求，其中page变量为当前点击的页上的数字。
                    $.ajax({
                        url: '/api/formModel/list',
                        type: 'POST',
                        data: {'page': page, 'size':10},
                        success: function (result) {
                            $('#formModelList tbody').empty();
                            let formListItem="";
                            for(let l of result.list){
                                let createUserName=l.createUserName?l.createUserName:"";
                                let updateUserName=l.updateUserName?l.updateUserName:"";
                                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                formListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.formName}</td>
                    <td>${l.instruction}</td>
                    <td>${createUserName}</td>
                    <td>${createAt}</td>
                    <td>${updateUserName}</td>
                    <td>${updateAt}</td>
                    <td>
                        <a href="/updateformModel?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <a style="margin-bottom:5px;margin-right:10px;" onclick="preview('${l._id}','formModel')" class="btn btn-info"> 查看</a>
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','formModel')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
                            }
                            $('#formModelList tbody').append(formListItem);
                            // $('.totalPages').text(result.totalPages)
                        }
                    })
                }
            });
        }
    })
});
function preview(id,name) {
    $("#preview"). modal();
    $("#preview .modal-body").empty();
    $.ajax({
        type : "POST",
        url : "/api/"+name+"/info",
        data : {
            id:id
        },
        success: function (result) {
            //定义表单字段数组
            let formFields = result.formFields;
            for (let f of formFields) {
                let formDisplay = '';
                if (f.fieldType.split("-")[0] == "input") {
                    if (f.fieldType.split("-")[1] === "radio") {
                        let option = "";
                        for (let o of f.fieldOptions) {
                            option += `<label class="radio-inline"><input type="radio" value=${o}>${o}</label>`
                        }
                        let leftHtml = `<div class="form-group ${f.column}"><label class="control-label">${f.displayName}</label><div class="radio">`;
                        let rightHtml = `</div></div>`;
                        formDisplay = leftHtml + option + rightHtml;
                    } else if (f.fieldType.split("-")[1] === "checkbox") {
                        let option = "";
                        for (let o of f.fieldOptions) {
                            option += `<label class="checkbox-inline"><input type="checkbox" value=${o}>${o}</label>`
                        }
                        let leftHtml = `<div class="form-group ${f.column}"><label class="control-label">${f.displayName}</label><div class="checkbox">`;
                        let rightHtml = `</div></div>`;
                        formDisplay = leftHtml + option + rightHtml;
                    } else if (f.fieldType.split("-")[1] === "file") {
                        formDisplay = `<div class="form-group ${f.column}"><label class="control-label">${f.displayName}</label><input type='${f.fieldType.split("-")[1]}'></div>`;
                    } else {
                        formDisplay = `<div class="form-group ${f.column}"><label class="control-label">${f.displayName}</label><input type='${f.fieldType.split("-")[1]}' class="form-control" placeholder=${f.placeholder}></div>`;
                    }
                    $("#preview .modal-body").append(formDisplay);
                } else if (f.fieldType.split("-")[0] == "textarea") {
                    formDisplay = `<div class="form-group ${f.column}"><label class="control-label">${f.displayName}</label><textarea class="form-control" rows="3" placeholder=${f.placeholder}></textarea></div>`;
                    $("#preview .modal-body").append(formDisplay);
                } else if (f.fieldType.split("-")[0] == "select") {
                    let option = "";
                    for (let o of f.fieldOptions) {
                        option += `<option value=${o}> ${o}</option>`
                    }
                    let leftHtml = `<div class="form-group ${f.column}"><label class="control-label">${f.displayName}</label><select class="form-control">`;
                    let rightHtml = `</select></div>`;
                    formDisplay = leftHtml + option + rightHtml;
                    $("#preview .modal-body").append(formDisplay);
                }else if(f.fieldType.split('-')[0]==='editor'){//富文本编辑器
                    formDisplay=`<div class="form-group ${f.column}"><label>${f.displayName}</label><div id="editor${f.fieldid}"></div></div>`
                    $("#preview .modal-body").append(formDisplay);
                    //创建富文本编辑器
                    var E = window.wangEditor;
                    var editor = new E('#editor'+f.fieldid);
                    editor.customConfig.menus = [
                        'head',
                        'bold',
                        'italic',
                        'fontSize',  // 字号
                        'fontName',
                        'underline',
                        'strikeThrough',  // 删除线
                        'foreColor',  // 文字颜色
                        'backColor',  // 背景颜色
                        'link',  // 插入链接
                        'justify',  // 对齐方式
                        'image',  // 插入图片
                        'table',  // 表格
                    ]
                    editor.create();
                }
            }
            $("#cancel-preview").click(function () {
                $("#myModal").modal('hide');
            })
        },
        error:function (error) {
            console.log(error)
        }
    });
}