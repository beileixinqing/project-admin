$(function () {
    let totalPages=0;
    let currentPage=1;
    let numberOfPages=8;
    $.ajax({
        url: '/api/projectModel/list',
        type: 'POST',
        data: {page: 1, size: 10},
        success: function (result) {
            $('#projectModelList tbody').empty();
            let projectModelListItem="";
            if(result.total<=0){
                projectModelListItem+=`<tr>
                    <td colspan="8" style="text-align: center">暂无数据</td>
                    </tr>`;
                $('#projectModelList tbody').append(projectModelListItem);
                return false;
            }
            for(let l of result.list){
                let createUserName=l.createUserName?l.createUserName:"";
                let updateUserName=l.updateUserName?l.updateUserName:"";
                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                projectModelListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.title}</td>
                    <td>${l.instruction}</td>
                    <td>${createUserName}</td>
                    <td>${createAt}</td>
                    <td>${updateUserName}</td>
                    <td>${updateAt}</td>
                    <td>
                        <a href="/updateprojectModel?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <!--<a style="margin-bottom:5px;margin-right:10px;" onclick="preview('${l._id}','projectModel')" class="btn btn-info"> 查看</a>-->
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','projectModel')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
            }
            $('#projectModelList tbody').append(projectModelListItem);
            totalPages=result.totalPages;
            currentPage=result.currentPage;
            // $('.totalPages').text(result.totalPages)
            $('#pageLimit').bootstrapPaginator({
                currentPage: currentPage,
                totalPages: totalPages,
                size:'normal',
                bootstrapMajorVersion: 3,
                alignment:"left",
                numberOfPages:numberOfPages,
                itemTexts: function (type, page, current) {
                    switch (type) {
                        case "first": return "首页";
                        case "prev": return "上一页";
                        case "next": return "下一页";
                        case "last": return "末页";
                        case "page": return page;
                    }//默认显示的是第一页。
                },
                onPageClicked: function (event, originalEvent, type, page) {//给每个页眉绑定一个事件，其实就是ajax请求，其中page变量为当前点击的页上的数字。
                    $.ajax({
                        url: '/api/projectModel/list',
                        type: 'POST',
                        data: {'page': page, 'size':10},
                        success: function (result) {
                            $('#projectModelList tbody').empty();
                            let projectModelListItem="";
                            for(let l of result.list){
                                let createUserName=l.createUserName?l.createUserName:"";
                                let updateUserName=l.updateUserName?l.updateUserName:"";
                                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                projectModelListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.title}</td>
                    <td>${l.instruction}</td>
                    <td>${createUserName}</td>
                    <td>${createAt}</td>
                    <td>${updateUserName}</td>
                    <td>${updateAt}</td>
                    <td>
                        <a href="/updateProjectModel?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <!--<a style="margin-bottom:5px;margin-right:10px;" onclick="preview('${l._id}','projectModel')" class="btn btn-info"> 查看</a>-->
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','projectModel')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
                            }
                            $('#projectModelList tbody').append(projectModelListItem);
                            // $('.totalPages').text(result.totalPages)
                        }
                    })
                }
            });
        }
    })
});
