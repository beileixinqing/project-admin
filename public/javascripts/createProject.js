//表单验证初始化
$(function () {
    $('form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            projectName: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '项目名称不能为空'
                    }
                }
            },
            instruction: {
                validators: {
                    notEmpty: {
                        message: '项目介绍不能为空'
                    }
                }
            },
            projectModel: {
                validators: {
                    notEmpty: {
                        message: '请选择项目模板'
                    }
                }
            }
        }
    });
});

/************ 创建项目页面 *************/
//选择项目模版,默认选择了状态一，以及对应的表单和表格模版
function selectProjectModel(value) {
    if (value) {
        let id=value.split(',')[0];
        $.ajax({
            type: "GET",
            url: "/api/project/renderStatusList",
            data: {
                id: id
            },
            success: function (result) {
                $("input[name='statusName']").val(result[0].statusName);
                $("input[name='statusId']").val(result[0]._id);
                formModels=result[0].formModels;
                tableModels=result[0].tableModels;
            },
            error: function (result) {
                console.error(result)
            }
        });
    }
}
//创建项目模版提交
function createProject() {
    $("#createProject").data("bootstrapValidator").validate();
    let validFlag = $("#createProject").data("bootstrapValidator").isValid();
    if(validFlag){
        $("#createProject").serializeArray();
        let m = {};
        $.each($("#createProject").serializeArray(), function(i, field){
            m[field.name]=field.value;
        });
        m.createUserName=getCookie('username');
        m.createUserId=getCookie('userId');
        m.formModels=formModels;
        m.tableModels=tableModels;
        console.log(m);
        $.ajax({
            type: "POST",
            url: "/api/project/create",
            data: {
                m,
                formModels,
                tableModels
            },
            success: function (result) {
                if(result.result===true){
                    $('#alert-success').show();
                    setTimeout(()=>{
                        $('#alert-success').hide();
                        window.location.href="/projectList"
                    },1000)
                }else{
                    $('#alert-danger').show();
                    setTimeout(()=>{
                        $('#alert-danger').hide();
                        window.location.href="/projectList"
                    },1000)
                }
            },
            error: function (result) {
                console.error(result)
                $('#alert-danger').show();
                setTimeout(()=>{
                    $('#alert-danger').hide();
                },3000)
            }
        });
    }
}


