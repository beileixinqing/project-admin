$(function () {
    let totalPages=0;
    let currentPage=1;
    let numberOfPages=8;
    $.ajax({
        url: '/api/project/list',
        type: 'POST',
        data: {page: 1, size: 10},
        success: function (result) {
            $('#projectList tbody').empty();
            let projectListItem="";
            if(result.total<=0){
                projectListItem+=`<tr>
                    <td colspan="9" style="text-align: center">暂无数据</td>
                    </tr>`;
                $('#projectList tbody').append(projectListItem);
                return false;
            }
            for(let l of result.list){
                let createUserName=l.createUserName?l.createUserName:"";
                let updateUserName=l.updateUserName?l.updateUserName:"";
                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                projectListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.projectName}</td>
                    <td>${l.instruction}</td>
                    <td>${createUserName}</td>
                    <td>${createAt}</td>
                    <td>${updateUserName}</td>
                    <td>${updateAt}</td>
                    <td>${l.statusName}</td>
                    <td>
                        <a href="/updateproject?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <!--<a style="margin-bottom:5px;margin-right:10px;" onclick="preview('${l._id}','project')" class="btn btn-info"> 查看</a>-->
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','project')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
            }
            $('#projectList tbody').append(projectListItem);
            totalPages=result.totalPages;
            currentPage=result.currentPage;
            // $('.totalPages').text(result.totalPages)
            $('#pageLimit').bootstrapPaginator({
                currentPage: currentPage,
                totalPages: totalPages,
                size:'normal',
                bootstrapMajorVersion: 3,
                alignment:"left",
                numberOfPages:numberOfPages,
                itemTexts: function (type, page, current) {
                    switch (type) {
                        case "first": return "首页";
                        case "prev": return "上一页";
                        case "next": return "下一页";
                        case "last": return "末页";
                        case "page": return page;
                    }//默认显示的是第一页。
                },
                onPageClicked: function (event, originalEvent, type, page) {//给每个页眉绑定一个事件，其实就是ajax请求，其中page变量为当前点击的页上的数字。
                    $.ajax({
                        url: '/api/project/list',
                        type: 'POST',
                        data: {'page': page, 'size':10},
                        success: function (result) {
                            $('#projectList tbody').empty();
                            let projectListItem="";
                            for(let l of result.list){
                                let createUserName=l.createUserName?l.createUserName:"";
                                let updateUserName=l.updateUserName?l.updateUserName:"";
                                let createAt=l.createAt?moment(l.createAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                let updateAt=l.updateAt?moment(l.updateAt).format('YYYY-MM-DD HH:MM:SS'):"";
                                projectListItem+=`<tr>
                    <td><input type="checkbox" value=${l._id}/></td>
                    <td>${l.projectName}</td>
                    <td>${l.instruction}</td>
                    <td>${createUserName}</td>
                    <td>${createAt}</td>
                    <td>${updateUserName}</td>
                    <td>${updateAt}</td>
                    <td>${l.statusName}</td>
                    <td>
                        <a href="/updateProject?id=${l._id}" style="margin-right:10px;margin-bottom:5px;" class="btn btn-primary">编辑</a>
                        <!--<a style="margin-bottom:5px;margin-right:10px;" onclick="preview('${l._id}','project')" class="btn btn-info"> 查看</a>-->
                        <a style="margin-bottom:5px;" onclick="removeOne(this,'${l._id}','project')" class="btn btn-default"> 删除</a>
                    </td>
                    </tr>`
                            }
                            $('#projectList tbody').append(projectListItem);
                            // $('.totalPages').text(result.totalPages)
                        }
                    })
                }
            });
        }
    })
});
function preview(id) {
    $.ajax({
        type : "POST",
        url : "/api/user/Info",
        data : {
            id:id
        },
        success : function(result) {
            let html='';
            let createAt=result['createAt']?moment(result['createAt']).format('YYYY-MM-DD HH:MM:SS'):"";
            let updateAt=result['updateAt']?moment(result['updateAt']).format('YYYY-MM-DD HH:MM:SS'):"暂无更新";
            let userType=result['userType']?result['userType']:"";
            html+=`<p><span>用户名：</span>${result['username']}</p>`;
            html+=`<p><span>手机号：</span>${result['mobile']}</p>`;
            html+=`<p><span>备注：</span>${result['remarks']}</p>`;
            html+=`<p><span>创建时间：</span>${createAt}</p>`;
            html+=`<p><span>更新时间：</span>${updateAt}</p>`;
            // html+=`<p><span>用户类型：</span>${userType}</p>`;
            $("#preview .modal-body").html(html);
            $("#preview"). modal();
        },
        error:function (result) {
            $("#myModal"). modal('hide');
        }
    });
}
