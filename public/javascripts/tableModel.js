//编辑项目模版页面默认加载状态列表数据
let data=new Array();
let tableList=new Array();
let tableId=0;
//表单验证初始化
$(function () {
    $('#tableModel').bootstrapValidator({
        // message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            tableName: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '表格模板名称不能为空'
                    }
                }
            },
            instruction: {
                validators: {
                    notEmpty: {
                        message: '项目模板介绍不能为空'
                    }
                }
            },
            projectModel: {
                validators: {
                    notEmpty: {
                        message: '请选择项目模板'
                    }
                }
            }
        }
    });
    $('#tableItem').bootstrapValidator({
        // message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            headerName: {
                // message: '用户名验证失败',
                validators: {
                    notEmpty: {
                        message: '表头名称不能为空'
                    }
                }
            },
            /*fieldName: {
                validators: {
                    notEmpty: {
                        message: '表头字段名不能为空'
                    }
                }
            },*/
            fieldType: {
                validators: {
                    notEmpty: {
                        message: '请选择数据类型'
                    }
                }
            }
        }
    });
});

function clone(arr) {
    var len = arr.length,
        arr = [];
    for(var i = 0;i < len;i++) {
        if(typeof arr[i] !== "object") {
            arr.push(arr[i]);
        } else {
            arr.push(arr[i].clone());
        }
    }
    return arr;
}
if($("#tableModelInfo").val()){
    tableList=JSON.parse($("#tableModelInfo").val()).table;
    for(let s of tableList){
        tableId+=1
        s['tableId']=tableId
    }
    data=$.extend(true, [], tableList);
    for(let value of data){
        delete value._id;
        value.tool=`<a style="margin-bottom:5px;" onclick="removeTableItem(this,tableId)" class="btn btn-default"> 删除</a>`;
    }
}
let t = $('#tableModelTable').DataTable({
    data:data,
    columnDefs: [
        {
            // targets: [ 0, 1, 2 ],
            className: 'mdl-data-table__cell--non-numeric'
        }
    ],
    columns: [
        /*{
            data: 'checked', // 返回json数据中的name
            title: '选择', // 表格表头显示文字
        },*/
        {
            data: 'tableId', // 返回json数据中的name
            title: 'tableId', // 表格表头显示文字
            visible:false,
        },
        {
            data: 'headerName', // 返回json数据中的name
            title: '表头名称', // 表格表头显示文字
            orderable:false,
        },
        /*{
            data: 'fieldName', // 返回json数据中的name
            title: '表头字段名称', // 表格表头显示文字
            orderable:false,
        },*/
        {
            data: 'fieldType', // 返回json数据中的name
            title: '数据类型', // 表格表头显示文字
        },
        {
            data: 'tool', // 返回json数据中的name
            title: '操作', // 表格表头显示文字
            orderable:false,
        },
    ]
});
//状态排序
let compare = function (obj1, obj2) {
    var val1 = obj1.statusOrder;
    var val2 = obj2.statusOrder;
    if (val1 < val2) {
        return -1;
    } else if (val1 > val2) {
        return 1;
    } else {
        return 0;
    }
}
//表单重置
$("#btn_add").click(function () {
    $('#myModal').modal();
    $("#tableItem")[0].reset();
})
function addTable() {
    tableId+=1;
    $("#tableItem").data("bootstrapValidator").validate();
    let validFlag = $("#tableItem").data("bootstrapValidator").isValid();
    if(validFlag){
        let data = {
            // checked:`<input type="checkbox" value=${tableId}/>`,
            tableId:tableId,
            headerName: $('#headerName').val(),
            // fieldName: $('#fieldName').val(),
            fieldType: $('#fieldType').find("option:selected").val(),
            _csrf:$('input[name="_csrf"]').val(),
            tool : `<a style="margin-bottom:5px;" onclick="removeTableItem(this,tableId)" class="btn btn-default"> 删除</a>`
        };
        t.row.add(data).draw( false );
        $('#myModal').modal('hide');
        delete data['tool']
        tableList.push(data);
        console.log(data)
    }
}
function createTableModel() {
    $("#tableModel").data("bootstrapValidator").validate();
    let validFlag = $("#tableModel").data("bootstrapValidator").isValid();
    if(validFlag){
        if(tableList.length<=0){
            $('#tableModelTable_wrapper').after(`<small class="help-block" style="color:#a94442;">表头字段不能为空</small>`);
            return false
        }
        let tableName=$('#tableModel input[name="tableName"]').val();
        let instruction=$('#tableModel textarea[name="instruction"]').val();
        let _csrf=$('#tableModel input[name="_csrf"]').val();
        if(tableList.length!==0){
            tableList.sort(compare);
        }
        for(let s of tableList){
            delete s['tableId']
        }
        const tableModelData={
            tableName:tableName,
            instruction:instruction,
            _csrf:_csrf,
            table:tableList,
            createUserName:getCookie('username'),
            createUserId:getCookie('userId'),
            status:1
        }
        console.log(tableModelData)
        $.ajax({
            type : "POST",
            url : "/api/tableModel/create",
            data : tableModelData,
            success : function(result) {
                $('#alert-success').show();
                setTimeout(()=>{
                    $('#alert-success').hide();
                    window.location.href="/tableModelList"
                },1000)
            },
            error:function (result) {
                $('#alert-alert').show();
                setTimeout(()=>{
                    $('#alert-alert').hide();
                },3000)
            }
        });
    }
}
function updateTableModel() {
    $("#tableModel").data("bootstrapValidator").validate();
    let validFlag = $("#tableModel").data("bootstrapValidator").isValid();
    if(validFlag){
        if(tableList.length<=0){
            $('#tableModelTable_wrapper').after(`<small class="help-block" style="color:#a94442;">表头字段不能为空</small>`);
            return false
        }
        let tableName=$('#tableModel input[name="tableName"]').val();
        let instruction=$('#tableModel textarea[name="instruction"]').val();
        let _csrf=$('#tableModel input[name="_csrf"]').val();
        let createUserName=$('#tableModel input[name="createUserName"]').val();
        let createUserId=$('#tableModel input[name="createUserId"]').val();
        let id=JSON.parse($("#tableModelInfo").val())._id;
        if(tableList.length>0){
            tableList.sort(compare);
        }
        $.ajax({
            type : "POST",
            url : "/api/tableModel/update",
            data : {
                id:id,
                status:0
            },
            success : function(result) {
                console.log(result)
            },
            error:function (result) {
                console.log(result)
            }
        });
        for(let s of tableList){
            delete s['tableId']
        }
        const tableModelData={
            tableName:tableName,
            instruction:instruction,
            _csrf:_csrf,
            table:tableList,
            createUserName:createUserName,
            createUserId:createUserId,
            updateUserName:getCookie('username'),
            updateUserId:getCookie('userId'),
            status:1
        }
        console.log(tableModelData)
        $.ajax({
            type : "POST",
            url : "/api/tableModel/updateIteration",
            data : tableModelData,
            success : function(result) {
                $('#alert-success').show();
                setTimeout(()=>{
                    $('#alert-success').hide();
                    window.location.href="/tableModelList"
                },1000)
            },
            error:function (result) {
                $('#alert-alert').show();
                setTimeout(()=>{
                    $('#alert-alert').hide();
                },3000)
            }
        });
    }
}
//删除单条tableItem
function removeTableItem(e,tableId) {
    $("#deleteModal .modal-body p").html("确认删除该条数据吗？");
    $("#deleteModal"). modal();
    $("#delete").click(function () {
        $("#deleteModal"). modal('hide');
        let data=$(e);
        t.row($(e).parents('tr')).remove().draw();
        for(let s of tableList){
            console.log(s)
            if(s['tableId']===tableId){
                tableList.remove(s)
                console.log(tableList)
            }
        }
    })
}
//把datatable警告置空
$.fn.dataTable.ext.errMode = 'none';
