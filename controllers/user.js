'use strict'
const mongoose = require('mongoose')
const User = mongoose.model('User')
const userDao = require('../dbDao/user')
const passport = require('../config/passport.js')
const log4js = require('koa-log4')
const logger = log4js.getLogger('auth')
const md5 = require('md5')
//渲染注册新用户页面
exports.renderAddUser = async (ctx) => {
    await ctx.render('addUser', {
        title: '添加员工帐号',
        username: ctx.session.username,
        csrf: ctx.csrf
    })
}
//渲染登录页面
exports.renderLogin = async (ctx) => {
    await ctx.render('login', {
        title: '请登录',
        csrf: ctx.csrf,
        username: ctx.session.username,
        password: ctx.session.password,
        remember: ctx.session.remember,
    })
}
//渲染编辑用户页面
exports.renderUpdateUser = async (ctx) => {
    let userInfo=await userDao.userInfo(ctx.request.query.id)
    console.log(userInfo)
    await ctx.render('updateUser', {
        title: '编辑员工信息',
        username: ctx.session.username,
        csrf: ctx.csrf,
        userInfo
    })
}
//渲染修改密码页面
exports.renderModifyPwd = async (ctx) => {
    await ctx.render('modifyPwd', {
        title: '修改密码',
        username: ctx.session.username,
        csrf: ctx.csrf
    })
}
//渲染员工列表页面
exports.renderUserList = async (ctx, next) => {
    await ctx.render('userList', {
        title: '员工列表',
        csrf: ctx.csrf,
        username: ctx.session.username
    })
    // let url = ctx.request.url
    // ctx.redirect('/login?redirect='+url)
}
/*************************** api ************************************/
/**
 * 注册新用户api
 * @param {type} userinfo [description]
 * @return {[type]}  userinfo [description]
 */
exports.addUser = async (ctx, next) => {
    let user=await userDao.findByName(ctx.request.body.username)
    if (user !== null) {
        ctx.body={
            msg: '用户已存在',
            data:0
        }
    } else {
        let res=await userDao.addUser(ctx.request.body)
        if (res) {
            ctx.body = {
                status:1,
                msg: '注册成功',
                data: res
            }
        } else {
            ctx.body = {
                status:0,
                msg: '注册失败',
                data: 0
            }
        }
    }
}
/**
 * 更新用户信息操作
 * @param  {[type]}   ctx  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.updateUser = async (ctx, next) => {
    let res=await userDao.updateUser(ctx.request.body)
    ctx.body = res
}
/**
 * 更新用户信息操作
 * @param  {[type]}   ctx  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.login = function (ctx, next) {
    if(ctx.request.body.remember){
        ctx.session.remember='checked'
        ctx.session.username=ctx.request.body.username
        ctx.session.password=ctx.request.body.password
    }else{
        ctx.session.remember=''
        ctx.session.username=''
        ctx.session.password=''
    }
    return passport.authenticate('local', function (err, user, info, status) {
        if (user) {
            ctx.body = {
                msg:'success',
                info:info,
                data:user,
                // url:url
            }
            return ctx.login(user)
        } else {
            ctx.body = {
                msg:'failed',
                info:info,
                // url:url
            }
        }
    })(ctx, next)
}
/**
 * 更新用户信息操作
 * @param  {[type]}   ctx  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.logout = function (ctx, next) {
    ctx.logout()
    ctx.cookies.set('username','')
    ctx.cookies.set('userId','')
    ctx.redirect('/login')
}
/**
 * 更新用户信息操作
 * @param  {[type]}   ctx  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.deleteUser = async (ctx, next) => {
    const result = await userDao.deleteUser(ctx.request.body.id)
    ctx.body = {
        result
    }
}
/**
 * 更新用户信息操作
 * @param  {[type]}   ctx  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.deleteManyUser = async (ctx, next) => {
    const result = await userDao.deleteManyUser(ctx.request.body.idArr)
    ctx.body = {
        result
    }
}
//展示员工信息
exports.userInfo = async (ctx) => {
    ctx.body=await userDao.userInfo(ctx.request.body.id)
}
//修改密码
exports.modifyPwd = async (ctx) => {
    ctx.body=await userDao.modifyPwd(ctx.request.body)
}
//分页查询用户列表
exports.list = async (ctx) => {
    let list = await userDao.list(Number(ctx.request.body.page), Number(ctx.request.body.size))
    ctx.body={
        list: list.docs,
        totalPages: list.pages,
        size: list.limit,
        total: list.total,
        currentPage:list.page
    }
}