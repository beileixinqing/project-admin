'use strict'
// 用于封装controllers的公共方法

exports.hasBody = async (ctx, next) => {
    let body = ctx.request.body || {}
    if (Object.keys(body).length === 0) {
        ctx.body = {
            success: false,
            err: '某参数缺失'
        }
        return next
    }
    await next()
}
