'use strict'
const log4js = require('koa-log4')
const logger = log4js.getLogger('project')
const tableModelDao = require('../dbDao/tableModel')
/**
 * 渲染表格模版页面
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.renderTableModel = async (ctx) => {
    await ctx.render('createTableModel', {
        title: '创建表格模板',
        username: ctx.session.username,
        csrf: ctx.csrf
    })
}
exports.renderUpdateTableModel = async (ctx) => {
    console.log(ctx.request.query.id)
    let tableModelInfo = await tableModelDao.tableModelInfo(ctx.request.query.id)
    await ctx.render('updateTableModel', {
        title: '编辑表格模板',
        username: ctx.session.username,
        csrf: ctx.csrf,
        tableModelInfo
    })
}
exports.tableModelInfos = async (ctx) => {
    let tableModelInfos = await tableModelDao.tableModelInfos(ctx.request.body.idArr)
    ctx.body=   tableModelInfos
}
/**
 * 创建表格模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.createTableModel = async (ctx, next) => {
    const result=await tableModelDao.createTableModel(ctx.request.body)
    ctx.body={
        result
    }
}
//表单模版列表查询
exports.tableList = async() => {
    let tableModelList = await tableModelDao.find({},function(err, doc) {
        console.log(doc)
    })
    return tableModelList
}
/**
 * 更新表格模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.updateTableModel = async (ctx, next) => {
    const result=await tableModelDao.update(ctx.request.body)
    ctx.body={
        result
    }
}
//迭代更新api
exports.updateIterationTableModel = async (ctx) => {
    const result=await tableModelDao.updateIterationTableModel(ctx.request.body)
    ctx.body={
        result
    }
}
/**
 * 删除表格模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.deleteTableModel = async (ctx, next) => {
    const result=await tableModelDao.deleteTableModel(ctx.request.query.id)
    ctx.body={
        result
    }
}
/**
 * 删除表格模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.deleteManyTableModel = async (ctx, next) => {
    const result=await tableModelDao.deleteManyTableModel(ctx.request.body.idArr)
    ctx.body={
        result
    }
}
/**
 *渲染表格模版列表页面
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.renderTableModelList = async (ctx) => {
    await ctx.render('tableModelList', {
        csrf: ctx.csrf
    })
}
//展示表单模版信息api
exports.tableModelInfo = async (ctx) => {
    console.log(ctx.request.body.id)
    ctx.body=await tableModelDao.tableModelInfo(ctx.request.body.id)
}
//分页查询表格列表
exports.list = async (ctx) => {
    let list = await tableModelDao.list(Number(ctx.request.body.page), Number(ctx.request.body.size))
    ctx.body={
        list: list.docs,
        totalPages: list.pages,
        size: list.limit,
        total: list.total,
        currentPage:list.page
    }
}
