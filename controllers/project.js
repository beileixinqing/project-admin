'use strict'
const projectDao = require('../dbDao/project')
const tableModelDao = require('../dbDao/tableModel')
const formModelDao = require('../dbDao/formModel')
const projectModelDao = require('../dbDao/projectModel')
const projectDataDao = require('../dbDao/projectData')
const OSS = require('ali-oss').Wrapper
const multiparty = require('multiparty')
const co = require('co')


let client = new OSS({
    region: 'oss-cn-qingdao',
    accessKeyId: 'LTAIBcDd9sXlrZV1',
    accessKeySecret: 'FAH7GFfzZ0F5EiR24IOYdHSzW9F6nh',
    bucket: 'p-adm-test'
})
//findAllById 通过id编辑前查询所有信息
exports.findAllById = async (ctx) => {
    //定义返回的空对象
    let result = {}
    //根据项目id查询项目基本信息
    let project = await projectDao.find(ctx.request.query)
    //返回项目信息
    result.project = project[0]
    //根据模版查询模版
    let projectModel = await projectModelDao.find({'_id': project[0].projectModelId})
    //返回项目模版信息
    result.projectModel = projectModel
    //遍历流程列表
    for (let i = 0; i < projectModel[0].statusList.length; i++) {
        let status = projectModel[0].statusList[i]
        //遍历表单列表
        for (let i = 0; i < status.formModels.length; i++) {
            //根据项目id和表单模版id查询数据表获取表单数据
            let formData = await projectDataDao.find({
                'projectSubmoduleId': status.formModels[i]._id,
                'projectId': result.project._id
            })
            if (formData[0]) {
                //返回表单数据信息
                result['formData'] = formData[0]
            }
        }
        //遍历表格列表
        for (let i = 0; i < status.tableModels.length; i++) {
            //根据项目id和表格模版id查询数据表获取表格数据
            let tableData = await projectDataDao.find({
                'projectSubmoduleId': status.tableModels[i]._id,
                'projectId': result.project._id
            })
            if (tableData[0]) {
                //返回表格数据信息
                result['tableData'] = tableData[0]
            }
        }
    }
    ctx.body = {
        result
    }
}

/**
 * 注册新用户
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
//默认渲染的时候查询项目模版列表
exports.renderProject = async (ctx) => {
    let list = await projectDao.projectModelList()
    await ctx.render('createProject', {
        title: '创建项目',
        list: list,
        username: ctx.session.username,
        csrf: ctx.csrf,
    })
}
//渲染编辑项目页面
exports.renderUpdateProject = async (ctx) => {
    // let projectInfo = await projectDao.projectInfo(ctx.request.query.id)
    // let tableModelList = await tableModelDao.tableModelList()
    //定义返回的空对象
    let projectInfo = {}
    //根据项目id查询项目基本信息
    let project = await projectDao.find(ctx.request.query.id)
    //返回项目信息
    projectInfo.project = project[0]
    //根据模版id查询模版
    let projectModel = await projectModelDao.find({'_id': project[0].projectModelId})
    //返回项目模版信息
    projectInfo.projectModel = projectModel[0]
    let modelData={}
    //遍历流程列表,获取status
    for (let status of projectModel[0].statusList) {
        //遍历表单列表
        for (let formModel of status.formModels) {
            //根据项目id和表单模版id查询数据表获取表单数据
            let formData = await projectDataDao.find({
                'projectSubmoduleId': formModel._id,
                'projectId': projectInfo.project._id
            })
            modelData[formModel._id]=formData
        }
        //遍历表格列表
        for (let tableModel of status.tableModels) {
            //根据项目id和表格模版id查询数据表获取表格数据
            let tableData = await projectDataDao.find({
                'projectSubmoduleId': tableModel._id,
                'projectId': projectInfo.project._id
            })
            modelData[tableModel._id]=tableData
        }
    }
    projectInfo.modelData = modelData
    console.log(projectInfo)
    await ctx.render('updateProject', {
        title: '编辑项目',
        csrf: ctx.csrf,
        username: ctx.session.username,
        projectInfo
    })
}
//渲染项目列表
exports.renderProjectList = async (ctx) => {
    let list = await projectDao.list(ctx.state.page.page, ctx.state.page.size)
    await ctx.render('projectList', {
        list: list,
        username: ctx.session.username,
        csrf: ctx.csrf
    })
}
//创建项目api
exports.createProject = async (ctx) => {
    const result = await projectDao.createProject(ctx.request.body)
    ctx.body = {
        result
    }
}
//更新项目api
exports.updateProject = async (ctx) => {
    const result = await projectDao.updateProject(ctx.request.body)
    ctx.body = {
        result
    }
}
//删除项目api
exports.deleteProject = async (ctx) => {
    const result = await projectDao.deleteProject(ctx.request.query.id)
    ctx.body = {
        result
    }
}
//批量删除项目api
exports.deleteManyProject = async (ctx) => {
    const result = await projectDao.deleteManyProject(ctx.request.body.idArr)
    ctx.body = {
        result
    }
}
//展示项目信息api
exports.projectInfo = async (ctx) => {
    ctx.body = await projectDao.projectInfo(ctx.request.body.id)
}

/**
 * 更新用户信息操作
 * @param  {[type]}   ctx  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.renderStatusList = async (ctx) => {
    let list = await projectDao.statusList(ctx.request.query.id)
    ctx.body = list
}
exports.renderFormModel = async (ctx) => {
    let formModel = await formModelDao.formModel(ctx.request.query.id)
    ctx.body = formModel
}
exports.renderTableModel = async (ctx) => {
    let tableModel = await tableModelDao.tableModelInfo(ctx.request.query.id)
    ctx.body = tableModel
}
exports.uploadFile = async (ctx, next) => {
    let alioss_uploadfile = function () {
        return new Promise(function (resolve, reject) {
            //上传多文件，使用multiparty
            let form = new multiparty.Form({
                encoding: 'utf-8',
                keepExtensions: true  //保留后缀
            })
            form.parse(ctx.req, async function (err, fields, files) {
                console.log(files)
                let data = []
                for (let f of files.file) {
                    // 文件名
                    let date = new Date()
                    let time = '' + date.getFullYear() + (date.getMonth() + 1) + date.getDate()
                    let filepath = 'project/' + time + '/' + date.getTime()
                    let fileext = f.originalFilename.split('.').pop()
                    let upfile = f.path
                    let newfile = filepath + '.' + fileext
                    await client.put(newfile, upfile).then((results) => {
                        console.log('文件上传成功!', results.url)
                        data.push(results.url)
                    }).catch((err) => {
                        console.log(err)
                    })
                }
                // ctx.response.type = 'json'
                ctx.response.body = {
                    errno: 0,
                    data: data
                }
                resolve(next())
            })
        })
    }
    await alioss_uploadfile()
}
exports.deleteFile = async (ctx,next) => {
    let alioss_deletefile = function() {
        return new Promise(function(resolve, reject) {
            co(function* () {
                client.useBucket('p-adm-test')
                var result = yield client.delete(ctx.request.body.path)
                console.log(result)
                // ctx.response.type = 'json'
                ctx.body = {
                    msg:'删除成功'
                }
            }).catch(function (err) {
                console.log(err)
            })
        })
    }
    await alioss_deletefile()
}
exports.downloadFile = async (ctx,next) => {
    let alioss_downloadfile = function() {
        return new Promise(function(resolve, reject) {
            co(function* () {
                client.useBucket('p-adm-test')
                let file = new File('/'+ctx.request.body.path)
                var result = yield client.get(ctx.request.body.path,file)
                console.log(result)
                // ctx.response.type = 'json'
                ctx.body = {
                    msg:'下载成功'
                }
            }).catch(function (err) {
                console.log(err)
            })
        })
    }
    await alioss_downloadfile()
}
exports.download = async (ctx,next) => {
    /*ctx.type = 'application/octet-stream'
    ctx.set('Content-Disposition','attachment;filename='+ctx.request.query.path.split('/')[ctx.request.query.path.split('/').length-1])*/
    await ctx.render('download', {
        path:ctx.request.query.path
    })
}
//分页查询表格列表
exports.list = async (ctx) => {
    let list = await projectDao.list(Number(ctx.request.body.page), Number(ctx.request.body.size))
    ctx.body={
        list: list.docs,
        totalPages: list.pages,
        size: list.limit,
        total: list.total,
        currentPage:list.page
    }
}