'use strict'
const projectDao = require('../dbDao/project')
const tableModelDao = require('../dbDao/tableModel')
const formModelDao = require('../dbDao/formModel')
const projectModelDao = require('../dbDao/projectModel')
const dao = require('../dbDao/projectData')

//findAllById 通过id编辑前查询所有信息
exports.findAllById = async (ctx) => {

   /* ctx.body = {
        result
    }*/
}
//findAllById 通过id编辑前查询所有信息
exports.find = async (ctx) => {

   /* ctx.body = {
        result
    }*/
}
//更新项目api
exports.add = async (ctx) => {
    const result = await dao.add(ctx.request.body)
    ctx.body = {
        result
    }
}
//更新项目api
exports.update = async (ctx) => {
    const result = await dao.update(ctx.request.body)
    ctx.body = {
        result
    }
}
//删除项目api
exports.delete = async (ctx) => {
    const result = await dao.delete(ctx.request.query.id)
    ctx.body = {
        result
    }
}
//批量删除项目api
exports.deleteMany = async (ctx) => {
    const result = await dao.deleteMany(ctx.request.body.idArr)
    ctx.body = {
        result
    }
}
