'use strict'
const log4js = require('koa-log4')
const logger = log4js.getLogger('project')
const formModelDao = require('../dbDao/formModel')
/****************** 渲染页面 ***********************/
/**
 * 渲染创建表单模版页面
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.renderFormModel = async (ctx) => {
    await ctx.render('createFormModel', {
        title: '创建动态表单模板',
        username: ctx.session.username,
        fieldList: [
            {label:'单行文本框',fieldType:'input-text'},
            {label:'多行文本框',fieldType:'textarea-textarea'},
            {label:'文章（可输入文字和图片）',fieldType:'editor-editor'},
            {label:'数字',fieldType:'input-number'},
            {label:'邮箱地址',fieldType:'input-email'},
            {label:'日期',fieldType:'input-date'},
            {label:'单选按钮',fieldType:'input-radio'},
            {label:'复选框(多选)',fieldType:'input-checkbox'},
            {label:'下拉列表',fieldType:'select-option'},
            {label:'上传图片',fieldType:'input-file-image'},
            {label:'上传文件',fieldType:'input-file-file'},
        ],
        csrf: ctx.csrf
    })
}
exports.renderUpdateFormModel = async (ctx) => {
    let formModelInfo = await formModelDao.formModelInfo(ctx.request.query.id)
    await ctx.render('updateFormModel', {
        title: '编辑动态表单模板',
        username: ctx.session.username,
        formModelInfo,
        fieldList: [
            {label:'单行文本框',fieldType:'input-text'},
            {label:'多行文本框',fieldType:'textarea-textarea'},
            {label:'文章（可输入文字和图片）',fieldType:'editor-editor'},
            {label:'数字',fieldType:'input-number'},
            {label:'邮箱地址',fieldType:'input-email'},
            {label:'日期',fieldType:'input-date'},
            {label:'单选按钮',fieldType:'input-radio'},
            {label:'复选框(多选)',fieldType:'input-checkbox'},
            {label:'下拉列表',fieldType:'select-option'},
            {label:'上传图片',fieldType:'input-file-image'},
            {label:'上传文件',fieldType:'input-file-file'},
        ],
        csrf: ctx.csrf
    })
}
/**
 *渲染表单模版列表页面
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.renderFormModelList = async (ctx) => {
    let list = await formModelDao.list(ctx.state.page.page, ctx.state.page.size)
    await ctx.render('formModelList', {
        list: list,
        username: ctx.session.username,
        csrf: ctx.csrf
    })
}
/**
 * 渲染预览文档页面
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.preview = async (ctx) => {
    console.log(ctx.request.query.path)
    await ctx.render('preview', {
        path:ctx.request.query.path
    })
}
/****************** api ***********************/
/**
 *创建表单模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.createFormModel = async (ctx) => {
    const result=await formModelDao.createFormModel(ctx.request.body)
    ctx.body={
        result
    }
}
/**
 *更新表单模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.updateFormModel = async (ctx) => {
    const result=await formModelDao.updateFormModel(ctx.request.body)
    ctx.body={
        result
    }
}
//条件查询api
exports.find = async (ctx) => {
    logger.info(ctx.request.query)
    const result=await formModelDao.find(ctx.request.query)
    ctx.body={
        result
    }
}
//迭代更新api
exports.updateIterationFormModel = async (ctx) => {
    const result=await formModelDao.updateIterationFormModel(ctx.request.body)
    ctx.body={
        result
    }
}
/**
 *删除表单模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.deleteFormModel = async (ctx) => {
    const result=await formModelDao.deleteFormModel(ctx.request.query.id)
    ctx.body={
        result
    }
}
/**
 *删除多个表单模版api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.deleteManyFormModel = async (ctx) => {
    const result=await formModelDao.deleteManyFormModel(ctx.request.body.idArr)
    ctx.body={
        result
    }
}
//展示表单模版信息api
exports.formModelInfo = async (ctx) => {
    ctx.body=await formModelDao.formModel(ctx.request.body.id)
}
//分页查询表格列表
exports.list = async (ctx) => {
    let list = await formModelDao.list(Number(ctx.request.body.page), Number(ctx.request.body.size))
    console.log(list)
    ctx.body={
        list: list.docs,
        totalPages: list.pages,
        size: list.limit,
        total: list.total,
        currentPage:list.page
    }
}