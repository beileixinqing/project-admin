'use strict'
const log4js = require('koa-log4')
const logger = log4js.getLogger('project')
const projectModelDao = require('../dbDao/projectModel')
const formModelDao = require('../dbDao/formModel')
const tableModelDao = require('../dbDao/tableModel')
/************** 渲染页面 *******************/
//渲染创建项目模版页面
exports.renderProjectModel = async (ctx) => {
    let formList = await formModelDao.formList()
    let tableModelList = await tableModelDao.tableModelList()
    await ctx.render('createProjectModel', {
        title: '创建项目模板',
        csrf: ctx.csrf,
        username: ctx.session.username,
        formList,
        tableModelList
    })
}
//渲染编辑项目模版页面
exports.renderUpdateProjectModel = async (ctx) => {
    let formList = await formModelDao.formList()
    let tableModelList = await tableModelDao.tableModelList()
    let projectModelInfo = await projectModelDao.projectModelInfo(ctx.request.query.id)
    await ctx.render('updateProjectModel', {
        title: '编辑项目模板',
        csrf: ctx.csrf,
        username: ctx.session.username,
        formList,
        projectModelInfo,
        tableModelList
    })
}
//渲染项目模版列表页面
exports.renderProjectModelList = async (ctx) => {
    await ctx.render('projectModelList', {
        username: ctx.session.username,
        csrf: ctx.csrf
    })
}
/****** api *******/
/**
 * 创建项目模版 api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.createProjectModel = async (ctx) => {
    const result=await projectModelDao.createProjectModel(ctx.request.body)
    ctx.body={
        result
    }
}
/**
 * 展示项目模版数据 api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.projectModelInfo = async (ctx) => {
    ctx.body=await projectModelDao.projectModelInfo(ctx.request.body.id)
}
/**
 * 更新项目模版 api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.updateProjectModel = async (ctx) => {
    const result=await projectModelDao.updateProjectModel(ctx.request.body)
    ctx.body={
        result
    }
}
//条件查询api
exports.find = async (ctx) => {
    logger.info(ctx.request.query)
    const result=await projectModelDao.find(ctx.request.query)
    ctx.body={
        result
    }
}
//迭代更新api
exports.updateIterationProjectModel = async (ctx) => {
    const result=await projectModelDao.updateIterationProjectModel(ctx.request.body)
    ctx.body={
        result
    }
}
/**
 * 删除项目模版 api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.deleteProjectModel = async (ctx) => {
    const result=await projectModelDao.deleteProjectModel(ctx.request.query.id)
    ctx.body={
        result
    }
}
/**
 * 删除多个项目模版 api
 * @param {Function} next          [description]
 * @yield {[type]}   [description]
 */
exports.deleteManyProjectModel = async (ctx) => {
    const result=await projectModelDao.deleteManyProjectModel(ctx.request.body.idArr)
    ctx.body={
        result
    }
}
//分页查询项目模板列表
exports.list = async (ctx) => {
    let list = await projectModelDao.list(Number(ctx.request.body.page), Number(ctx.request.body.size))
    ctx.body={
        list: list.docs,
        totalPages: list.pages,
        size: list.limit,
        total: list.total,
        currentPage:list.page
    }
}