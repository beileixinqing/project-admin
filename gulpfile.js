const gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
    less = require('gulp-less')

gulp.task('less', function () {
    gulp.src('./public/stylesheets/*.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(gulp.dest('./public/stylesheets'))
        .pipe(livereload())
})

gulp.task('watch', function () {
    gulp.watch('./public/stylesheets/*.less', ['less', function () {
        livereload.reload()
    }])
    gulp.watch('./views/*.pug', function () {
        livereload.reload()
    })
})

gulp.task('develop', function () {
    livereload.listen()
    nodemon({
        script: 'bin/www',
        ext: 'js',
        ignore : [
            'public/**',
            'views/**'
        ],
        stdout: false
    }).on('readable', function () {
        this.stdout.pipe(process.stdout)
        this.stderr.pipe(process.stderr)
    }).on('restart', () => {
        // Give it 1 sec to make sure server's done restartings
        setTimeout(() => {
            livereload.reload()
        }, 1000)
    })
})

gulp.task('default', [
    'less',
    'develop',
    'watch'
])
