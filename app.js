const config = require('./config/config')
const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const log4js = require('koa-log4')
const logger = log4js.getLogger('app')
const favicon = require('koa-favicon')
const session = require('koa-session')
// const passport = require('koa-passport')
const passport = require('./config/passport')
const CSRF = require('koa-csrf')

require('./config/db')
/**
 * 获取数据库表对应的js对象所在的路径
 * @type {[type]}
 */
const fs = require('fs')
const path = require('path')
const models_path = path.join(__dirname, '/models')
/**
 * 以递归的形式，读取models文件夹下的js模型文件，并require
 * @param  {[type]} modelPath [description]
 * @return {[type]}           [description]
 */
var walk = function (modelPath) {
    fs
        .readdirSync(modelPath)
        .forEach(function (file) {
            var filePath = path.join(modelPath, '/' + file)
            var stat = fs.statSync(filePath)
            if (stat.isFile()) {
                if (/(.*)\.(js|coffee)/.test(file)) {
                    require(filePath)
                }
            }
            else if (stat.isDirectory()) {
                walk(filePath)
            }
        })
}
walk(models_path)

// error handler
onerror(app)

// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}))
app.use(json())
// app.use(logger())
// app.use(log4js.koaLogger(log4js.getLogger('http'), {level: 'auto',format:':method :url'}))

app.use(favicon(config.root + '/public/favicon.ico'))
app.use(require('koa-static')(config.root + '/public'))
app.use(require('koa-static')(config.root + '/bower_components'))

app.use(views(config.root + '/views', {
    extension: 'pug'
}))

// Sessions
app.keys = ['project management system']
const CONFIG = {
    key: 'SESSIONID',
}
app.use(session(CONFIG, app))
//统计页面访问次数
/*app.use(ctx => {
 // ignore favicon
 if (ctx.path === '/favicon.ico') return
 let n = ctx.session.views || 0;
 ctx.session.views = ++n
 ctx.body = n + ' views'
 })*/
// passport
app.use(passport.initialize())
app.use(passport.session())

// page utils
app.use(async (ctx, next) => {
    ctx.state.page = {page: 1, size: 10}
    if (ctx.request.body.page !== undefined) {
        ctx.state.page.page = Number(ctx.request.body.page)
    }
    if (ctx.request.body.size !== undefined) {
        ctx.state.page.size = Number(ctx.request.body.size)
    }
    logger.debug(ctx.state.page)
    await next()
})

// add the CSRF middleware
app.use(new CSRF({
    invalidSessionSecretMessage: 'Invalid session secret',
    invalidSessionSecretStatusCode: 403,
    invalidTokenMessage: 'Invalid CSRF token',
    invalidTokenStatusCode: 403,
    excludedMethods: ['GET', 'HEAD', 'OPTIONS', 'POST'],
    disableQuery: false
}))

// logger
app.use(async (ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    logger.debug(`${ctx.method} ${ctx.url} - ${ms}ms`)

})
//定义允许直接访问的url
const allowpage = ['/login','/api/login']
//拦截
function localFilter(ctx) {
    let url = ctx.originalUrl
    if (allowpage.indexOf(url) > -1) {
        logger.info('当前地址需登录验证')
    }else {
        if (ctx.isAuthenticated()) {
            if(url==='/'){
                ctx.redirect('/projectList')
            }
            console.log('login status validate success')
        } else {
            console.log('login status validate fail')
            console.log(ctx.request.url)
            ctx.redirect('/login')
        }
    }
}
//session拦截
app.use(async (ctx, next) => {
    localFilter(ctx)
    await next()

})
// routes
const router = require('./config/router')()
app
    .use(router.routes())
    .use(router.allowedMethods())
// error-handling
app.on('error', (err, ctx) => {
    logger.error('server error', err, ctx)
})
module.exports = app