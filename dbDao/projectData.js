'use strict'

var mongoose = require('mongoose')
const Model = mongoose.model('ProjectData')

//增加
exports.add = async(data) => {
    let model = new Model(data)
    let flag = false
    await model.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.delete = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.update = async(data) => {
    let flag = false
    await Model.update({_id:data._id},data,function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//条件查询
exports.find = async (data) => {
    let list=await Model.find(data, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}
//所有查询
exports.findAll = async () => {
    let list=await Model.find({}, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}