'use strict'

var mongoose = require('mongoose')
const Project = mongoose.model('Project')
const ProjectData = mongoose.model('ProjectData')
const ProjectModel = mongoose.model('ProjectModel')

/**
 * 创建项目，把关联的表单和表格模版信息存入projectData数据表
 * 创建项目，把项目信息存入project数据表
 * @param  {[type]} options.phoneNumber [description]
 * @return {[type]}                     [description]
 */
exports.createProject =async(data) => {
    data.m.projectModelId=data.m.projectModel.split(',')[0]
    data.m.projectModelName=data.m.projectModel.split(',')[1]
    delete data.m.projectModel
    let flag=false
    const p1 =  await new Project(data.m).save()
    /*p1.then(function (info) {
        let arr=[]
        for(let v of data.formModels){
            arr.push(new ProjectData({projectId:info._id,projectSubmoduleId:v._id,createUserId:info.createUserId,createUserName:info.createUserName}).save())
        }
        for(let v of data.tableModels){
            arr.push(new ProjectData({projectId:info._id,projectSubmoduleId:v._id,createUserId:info.createUserId,createUserName:info.createUserName}).save())
        }
        Promise.all(arr)
        console.log('save success')
        flag=true
        // return flag
    }).catch(function(err) {
        // 最后的catch()方法可以捕获在这一条Promise链上的异常
        console.log('出错：' + err)// 出错：reject
        flag=false
        // return flag
    })*/
    if(p1){
        flag=true
    }else{
        flag=false
    }
    return flag
}
exports.updateProject = async(data) => {
    let flag = false
    for(let d of data.data){
        let info=await ProjectData.find({projectId:d.projectId,projectSubmoduleId:d.projectSubmoduleId})
        if(info.length>0){
            console.log(info)
            await ProjectData.update({projectId:d.projectId,projectSubmoduleId:d.projectSubmoduleId},d,function (err,res) {
                if (err) {
                    flag = false
                    console.error(err)
                }else{
                    flag=true
                    console.log("更新成功")
                    console.log(res)
                }
            })
        }else{
            let projectData=new ProjectData(d)
            await projectData.save(function (err,doc) {
                if (err) {
                    flag = false
                    console.error(err)
                }else{
                    flag=true
                    console.log("存储成功")
                    console.log(doc)
                }
            })
        }
    }
    // delete data.data
    // delete data._id
    await Project.update({_id:data._id},data,function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
exports.deleteProject = async(id) => {
    let flag = false
    await Project.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
exports.deleteManyProject = async(idArr) => {
    let flag = false
    await Project.remove({_id:{ $in: idArr}},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
exports.projectModelList = async () => {
    let list=await ProjectModel.find({status:1},null, {sort: {'createAt':-1}})
    return list
}
//展示项目信息
exports.projectInfo = async (id) => {
    let projectInfo = await Project.findById(id, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return projectInfo
}
exports.statusList = async (id) => {
    let list=await ProjectModel.findById({_id:id}, function (err, doc){
        if(err){
            // console.log(err)
        }else{
            // console.log(doc)
        }
    })
    return list.statusList
}

//项目模版列表分页查询
exports.list = async (page, limit) => {
    let list= await Project.paginate({}, {page: page, limit: limit,sort: {'createAt':-1}})
    return list
}
/*exports.formList = async (id) => {
    let list=await ProjectModel.findById({_id:id}, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}*/

const Model = mongoose.model('Project')

//增加
exports.add = async(data) => {
    let model = new Model(data)
    let flag = false
    await model.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.delete = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.update = async(data) => {
    let flag = false
    await Model.update({_id:data._id},data,function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//条件查询
exports.find = async (id) => {
    let list=await Model.find({_id:id}, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}