'use strict'
const mongoose = require('mongoose')
const User = mongoose.model('User')
const md5 = require('md5')
/**
 * 通过用户名查询该用户是否存在
 * @param  {[type]} username [description]
 * @return {[type]} result   [description]
 */
exports.findByName = async (username) => {
    let res = {}
    await User.findOne({username: username},function(err,user){
        if (err) {
            res = {}
        } else {
            res = user
        }
    })
    return res
}

/**
 * 用户列表列表查询
 * @return {[type]} [description]
 */
//项目模版列表查询
exports.list = async (page, limit) => {
    let list = await User.paginate({},{page: page, limit: limit})
    console.log(list)
    return list
}
/**
 * 增加用户
 * @param  {[User]} user [mongoose.model('User')]
 * @return {[type]}      [description]
 */
exports.addUser = async (user) => {
    user.password=md5(user.password)
    let userModel = new User(user)
    let res = await userModel.save()
    return res
}
/**
 * 更新用户信息
 * @param  {[type]} id  [description]
 * @return {[type]} result [description]
 */
exports.updateUser = async (data) => {
    let res = {}
    await User.update({_id:data.id},data,function (err) {
        if (err) {
            res = {
                msg:'修改失败，请重试',
                status:0
            }
            return console.error(err)
        }else{
            res = {
                msg:'修改成功！',
                status:1
            }
        }
    })
    return res
}

/**
 * 删除用户
 * @param  {[type]} id  [description]
 * @return {[type]} result [description]
 */
exports.deleteUser = async (id) => {
    let flag = false
    await User.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
/**
 * 删除多个用户
 * @param  {[type]}  id [description]
 * @return {[type]}  status [description]
 */
exports.deleteManyUser = async(idArr) => {
    let flag = false
    await User.remove({_id:{ $in: idArr}},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//展示用户信息api
exports.userInfo = async (id) => {
    let userInfo = await User.findById(id, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return userInfo
}
//修改密码api
exports.modifyPwd = async (data) => {
    let res = {}
    let user = await User.findOne({_id: data.id}, function (err, user) {
        if (err) {
            res = {
                msg: '查不到该用户，请重试',
                status: 0
            }
        }
    })
    if (md5(data.password) === user.password) {
        await User.update({_id: data.id}, {password: md5(data.newPassword)}, function (err) {
            if (err) {
                res = {
                    msg: '密码修改失败，请重试',
                    status: 0
                }
            } else {
                res = {
                    msg: '密码修改成功',
                    status: 1
                }
            }
        })
    } else {
        res = {
            msg: '原密码错误，请重试',
            status: 0
        }
    }
    return res
}
//增加
exports.add = async(data) => {
    let model = new User(data)
    let flag = false
    await model.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.delete = async(id) => {
    let flag = false
    await User.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.update = async(data) => {
    let flag = false
    await User.update({_id:data._id},data,function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//条件查询
exports.find = async (data) => {
    let list=await User.find(data, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}