'use strict'

const mongoose = require('mongoose')
const Model = mongoose.model('ProjectModel')
/**
 * 通过电话号码查询
 * @param  {[type]} options.phoneNumber [description]
 * @return {[type]}                     [description]
 */
exports.createProjectModel = async(data) => {
    let projectModel = new Model(data)
    let flag = false
    await projectModel.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.updateProjectModel = async(data) => {
    let flag = false
    await Model.update({_id:data.id},{status:data.status},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//迭代更新
exports.updateIterationProjectModel = async(data) => {
    let projectModel = new Model(data)
    let flag = false
    await projectModel.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.deleteProjectModel = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
exports.deleteManyProjectModel = async(idArr) => {
    let flag = false
    await Model.remove({_id:{ $in: idArr}},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//展示项目模版信息
exports.projectModelInfo = async (id) => {
    let projectModelInfo = await Model.findById(id, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return projectModelInfo
}
//项目模版列表分页查询
exports.list = async (page, limit) => {
    let list = await Model.paginate({status:1}, {page: page, limit: limit,sort: {'createAt':-1}})
    return list
}
//项目模板所有列表
exports.find = async (data) => {
    let list=await Model.find(data, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}

//增加
exports.add = async(data) => {
    let model = new Model(data)
    let flag = false
    await model.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.delete = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.update = async(data) => {
    let flag = false
    await Model.update({_id:data._id},data,function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}