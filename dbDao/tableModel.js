'use strict'

const mongoose = require('mongoose')
const Model = mongoose.model('TableModel')
/**
 * 创建表格模版
 * @param  {[type]} options.phoneNumber [description]
 * @return {[type]}                     [description]
 */
exports.createTableModel = async(data) => {
    let tableModel = new Model(data)
    var flag = false
    await tableModel.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//迭代更新
exports.updateIterationTableModel = async(data) => {
    let tableModel = new Model(data)
    let flag = false
    await tableModel.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
exports.tableModelInfo = async (id) => {
    let tableModelInfo = await Model.findById(id, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return tableModelInfo
}
/*exports.tableModelInfos = async (idArr) => {
    console.log(idArr)
    let tableModelInfos = await Model.find({_id:{ $in: idArr}}, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return tableModelInfos
}*/
exports.tableModelInfo = async (id) => {
    let tableModelInfo = await Model.findById(id, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return tableModelInfo
}
/**
 * 删除表格模版
 * @param  {[type]} options.phoneNumber [description]
 * @return {[type]}                     [description]
 */
exports.deleteTableModel = async(id) => {
    var flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
/**
 * 删除多个表格模版
 * @param  {[type]} options.phoneNumber [description]
 * @return {[type]}                     [description]
 */
exports.deleteManyTableModel = async(idArr) => {
    var flag = false
    await Model.remove({_id:{ $in: idArr}},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
exports.tableModelList = async (id) => {
    let list=await Model.find({status:1}, function (err, doc){
        if(err){
            // console.log(err)
        }else{
            // console.log(doc)
        }
    })
    return list
}
//表格模版列表mongoose分页查询
exports.list = async (page, limit) => {
    let list = await Model.paginate({status:1}, {page: page, limit: limit,sort: {'tableName':1}})
    return list
}
//增加
exports.add = async(data) => {
    let model = new Model(data)
    let flag = false
    await model.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.delete = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.update = async(data) => {
    let flag = false
    await Model.update({_id:data.id},{status:data.status},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//条件查询
exports.find = async (data) => {
    let list=await Model.find(data, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}