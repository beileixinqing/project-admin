'use strict'
const mongoose = require('mongoose')
const Model = mongoose.model('FormModel')

//创建表单模版
exports.createFormModel = async(data) => {
    let formModel = new Model(data)
    let flag = false
    await formModel.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新表单模版
exports.updateFormModel = async(data) => {
    let flag = false
    await Model.update({_id:data.id},{status:data.status},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//展示表单模版信息
exports.formModelInfo = async (id) => {
    let formModelInfo = await Model.findById(id, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return formModelInfo
}
//删除单个表单模版
exports.deleteFormModel = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除多个表单模版
exports.deleteManyFormModel = async(idArr) => {
    let flag = false
    await Model.remove({_id:idArr},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//表单模版列表查询
exports.formList = async() => {
    let formList = await Model.find({status:1},null, {sort: {'formName':1}})
    console.log(formList)
    return formList
}
//渲染表单模板
exports.formModel = async(id) => {
    let formModel = await Model.findOne({_id:id},null, {sort: {'tableName':1}})
    return formModel
}
//表单模版列表分页查询
exports.list = async (page, limit) => {
    let list = await Model.paginate({status:1},{page: page, limit: limit,sort: {'formName':1}})
    return list
}
//迭代更新
exports.updateIterationFormModel = async(data) => {
    let formModel = new Model(data)
    let flag = false
    await formModel.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//增加
exports.add = async(data) => {
    let model = new Model(data)
    let flag = false
    await model.save(function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//删除
exports.delete = async(id) => {
    let flag = false
    await Model.remove({_id:id},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//更新
exports.update = async(data) => {
    let flag = false
    await Model.update({_id:data._id},{$set:{status:data.status}},function (err) {
        if (err) {
            flag = false
            return console.error(err)
        }else{
            flag=true
        }
    })
    return flag
}
//条件查询
exports.find = async (data) => {
    let list=await Model.find(data, function (err, doc){
        if(err){
            console.log(err)
        }else{
            console.log(doc)
        }
    })
    return list
}
