投融资项目管理系统开发文档
====
This project shows ways to

+ Use mongodb in Koa2
+ Use mongoose to operate mongodb
+ Use mongoose-paginate to mongodb paginate
+ xss csrf
+ datatables for table
+ bootstrap-validate for validate
+ bootstrap-paginate for paginate
+ Ali Yun oss for upload file
+ less for css
+ bower for static resources reference
+ pug template engine
+ Ajax Login and Ajax Logout in Koa2, based on [koa-passport](https://github.com/rkusa/koa-passport) and [passport-local](https://github.com/jaredhanson/passport-local)
+ Export excel in Koa2, based on [excel-export](https://github.com/functionscope/Node-Excel-Export)
+ Serve static files in Koa2
+ Deploy Koa2 app with pm2
+ use koa-log4 for view log
+ use gulp for automated building

### Usage

```bash
$ npm install

# Start application
$ npm run dev
or
$ gulp

# deploy to the server
$ npm run pm2 -g

# start
$ pm2 start app.js

# view the process
$ pm2 list

# monit
$ pm2 monit

# update
$ pm2 reload all --update-env to update
```
### 项目结构
```bash
www                 项目
app.js              项目入口文件
config              配置文件
controllers         项目业务逻辑
dbDao               与数据库交互逻辑
logs                项目日志文件
models              项目数据结构定义
bower_components    bower引用的静态资源库目录
node_modules        npm包目录
public              静态资源文件
utils               工具包目录
views               项目页面模板
```
### 思路分析：
    管理员：
    管理员可以添加/管理项目模板
    管理员可以添加/管理表单模板
    管理员可以添加/管理表格模板
    管理员可以创建项目模板，可以增加流程
    增加流程可以选择表单模板、表格模板
    创建项目：
    创建项目，创建项目的时候可以选择项目模板
    创建项目模板的时候可以选择流程
    流程不同，展示的表单模板不同，即表格模板也相应变化
    动态创建表单的时候可以创建文本/数字/富文本/文件图片等类型 ，可以在线预览与下载文件
    然后提交项目
    编辑该项目，可以随时切换当前流程（即当前状态）并修改已填写的信息
    表单模版自动保存：监听整个表单模版数据变化，如有变化更新存数据的变量。
    表更新机制：迭代。由于涉及到用项目模版创建了项目，填写了表单和表格之后，模版会更改的情况，这样项目中填写的数据会失效，所以默认当项目模版更改之后，项目中的关联的模版没有更改，并且通过项目中关联的模版id可以查询到原来项目模版的信息，然后项目模版更改的时候，相当于创建一个新的项目模版，给项目模版一个状态值，当进行更改的时候，原来的状态值变为不展示，当前新创建的项目模版变为展示，这样所有的项目模版都是可以查询到的。不会影响关联问题。
    表单和表格模版创建存储机制：每创建一个表单模版会存在一个单独存放数据的表里，每创建一条表格数据也会存放在数据表里，项目编辑页面渲染数据的时候，根据项目模版id和项目id来查询到所有的表单模版和表格模版数据，再根据模版id展示到对应条目下。
### 项目难点
    项目中遇到的问题与难点：
    mongoose使用，与数据库交互，例如：
    mongoose增删改查问题
    mongoose联表查询问题
    mongoose子文档问题
    mongoose多条件查询
    bootstrap-table数据渲染的问题
    ajax和默认load加载不生效问题
    动态表单模板创建字段设计与表设计
    mongodb其他数据表设计
    动态表单数据填入之后存储问题
    项目模板更新，已经用了该模板创建的项目数据不一致问题
    所有编辑页面数据展示问题
    表单模版实时监听表单输入，实时预览实现
    表单模版实时监听表单输入，实时保存所填数据
    表单模版单选按钮、复选框、下拉框选项已选默认展示问题
    富文本插入，并支持上传文件到阿里云
    上传文件到阿里云并且支持多张上传问题
    上传文件预览
    上传文件下载
    上传图片预览
    koa-passport中间件使用
    判断是否登录全局拦截器
    async await用法与promise用法
    异步获取mongoose返回数据
    cookie与session设置与获取问题
    中间件选择问题
    项目结构设计
    封装公共函数
### 操作说明：
    分为模块：
    员工管理

    默认管理员账号：admin
    管理员密码：123
    管理员可以在员工管理中新建员工，修改员工信息，修改员工密码

    模版管理

    模版一共分为：表单模版、表格模版、项目模版
    表单模版与表格模版为平行关系；
    先建好需要的表单模版和表格模版，再建项目模版；
    项目模版里可以添加流程（状态），比如筛选阶段、投资阶段等；
    每个流程可以选择关联的即需要填写的表单模版和表格模版；
    流程顺序只能输入数字的1，2，3……；
    最终展示顺序按照流程顺序展示；
    项目模版也可以建立多个，不同的项目需要填写不同的模版信息；
    表单模版创建的时候，模版名称和介绍必填，中间一栏为表单模版样式预览，不可以填写内容，每创建一个表单组件则在左侧一栏选择，并在中间一栏选中，右侧一栏编辑表单组件信息，编辑完成后，数据会自动保存；
    项目模版、表格模版、表单模版默认机制：由于创建的项目已经关联对应的模版，所以如果更改了模版内容之后，对应的项目会出现问题，则模版模版修改之后，项目对应的模版信息不回更改，只有再重新使用该模版创建项目之后，新模版方可生效；

    项目管理

    建立好项目模版之后可以再项目管理里创建项目；
    首次创建项目只需要选择项目模版，模版默认选择流程一；
    在项目列表里可以选择编辑项目，编辑项目的时候可以填写对应的表单、表格信息并更新项目；
    编辑项目对应流程信息填写完成后，可以选择切换状态，继续填写信息；
### License

MIT


 